# Laundry Machine 

The software controller of a laundry machine is developed in C++. This machine can do laundry through 3 different washing programs: A, B, and C. They differ in requirements such as temperature, water level, duration, etc. Since it would be very inconvenient to carry out the whole development while connected to a real laundry machine, a Laundry Machine Simulator (LMS from now on) will be used as
hardware      

* Coins

Each of the washing programs has a cost, which the user will be able to pay through coins. The machine accepts three types of coins: 10, 50 and 200 cents. And it only accepts a limited amount of them: at most three of 10 and 50, and at most two of 200. Once the user has provided enough money for a specific washing program, the whole washing process can be started. 

* Door Lock and Soap

The switch, named �Door Lock�, is used to signal to the LMS whether the door has been closed or not.

Our laundry machine provides two compartments for soap, numbered 1 and 2, for prewash cycles and main wash cycles. The first compartment can store 2 units of soap, while the second one can store 3 units of soap. Each of the soap switches signals closing the compartment after providing all possible units of soap.

* Water 

There are several input/output lines on the flat cable related to the handling of water: for monitoring whether there is water pressure; for opening and closing the water inlet and outlet valves, called the drain and the sink; and for monitoring the amount of water in the machine tank.

* Temperature

The laundry machine has also a heater. There is an output line to turn it on and off, and two input lines to monitor the current water temperature. The heater can only be turned on if at least a third of the washing tank has water. Turning the heater on without water will caused immediate damage. The two input lines allow the reading of four different temperature levels: environment, warm,
warmer, hot.

* Motor 

There are also three output lines to control the rotation motor of the laundry machine. Two of them are for setting the motor at different speeds: off, low, medium, high. Low speed is for careful washing, medium for regular washing, and high speed is for centrifugal drying of clothes. A third line indicates whether the motor must rotate clockwise or counter clockwise.
 