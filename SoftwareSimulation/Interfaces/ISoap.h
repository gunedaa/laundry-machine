#ifndef ISOAP
#define ISOAP

#include <stdint.h>

class ISoap {

  public:
    virtual void SetSoap1(uint8_t setVal) = 0;
    virtual void SetSoap2(uint8_t setVal) = 0;
    virtual uint8_t GetSwitchSoap1() = 0;
    virtual uint8_t GetSwitchSoap2() = 0;
};

#endif
