#ifndef DIRECTOR_H
#define DIRECTOR_H

#include "Coins.h"
#include "ProgramSelector.h"
#include "Soap.h"
#include "Water.h"
#include "Door.h"
#include "Buzzer.h"
#include "ProgramInstructions.h"
#include "DirtSensor.h"
#include "Wash.h"



class Director
{
  public:
    Director(Coins *ic, ProgramSelector *ps, ProgramInstructions *ProgramInstructions, Soap *sp, Water *water, Door *door, Buzzer *buzzer, DirtSensor *dirtSensor, Motor *mr, Heater *hr);
    void Work();

  private:
    bool IsReady();
    Heater *ht;
    Motor *mt;
    Coins *c;
    ProgramSelector *p;
    Soap *s;
    Water *w;
    Door *d;
    Buzzer *b;
    ProgramInstructions *pi;
    DirtSensor *ds;
    Wash *wsh;
    ProgramOptions program;

    enum DirectorState {IDLE,CONSTRUCT ,RUN};

    DirectorState DS;


};

#endif
