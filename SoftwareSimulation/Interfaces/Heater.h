#ifndef HEATER_H
#define HEATER_H

#include "IHeater.h"
#include <stdint.h>

class Heater
{
  public:

    //constructor
    Heater(IHeater *heater);

    //this method handles heating up the water to given temp
    bool SetHeaterLevel(uint8_t temp);

    //this method handles turning off the heater
    bool StopHeat();

    //this method return the current temperature level
    uint8_t GetTemperatureLevel();

  private:
    IHeater *h;

};

#endif
