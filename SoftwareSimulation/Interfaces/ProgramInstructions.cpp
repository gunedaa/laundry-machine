#include "ProgramInstructions.h"
//#include <Arduino.h>

ProgramInstructions::ProgramInstructions()
{
  ProgramA.preWaterVal = 2;
  ProgramA.main1WaterVal = 2;
  ProgramA.main2WaterVal = 2;
  ProgramA.preTempVal = 0;
  ProgramA.main1TempVal = 2;
  ProgramA.main2TempVal = 0;
  ProgramA.dryTempVal = 0;
  ProgramA.preSpeedVal = 1;
  ProgramA.main1SpeedVal = 2;
  ProgramA.main2SpeedVal = 2;
  ProgramA.drySpeedVal = 3;
  ProgramA.main1Reps = 2;
  ProgramA.main2Reps = 2;
  ProgramA.dryReps = 2;
  ProgramA.progPrice = 360;


  ProgramB.preWaterVal = 2;
  ProgramB.main1WaterVal = 2;
  ProgramB.main2WaterVal = 2;
  ProgramB.preTempVal = 3;
  ProgramB.main1TempVal = 3;
  ProgramB.main2TempVal = 0;
  ProgramB.dryTempVal = 0;
  ProgramB.preSpeedVal = 1;
  ProgramB.main1SpeedVal = 2;
  ProgramB.main2SpeedVal = 2;
  ProgramB.drySpeedVal = 3;
  ProgramB.main1Reps = 2;
  ProgramB.main2Reps = 2;
  ProgramB.dryReps = 2;
  ProgramB.progPrice = 480;

  ProgramC.preWaterVal = 2;
  ProgramC.main1WaterVal = 3;
  ProgramC.main2WaterVal = 2;
  ProgramC.preTempVal = 2;
  ProgramC.main1TempVal = 3;
  ProgramC.main2TempVal = 0;
  ProgramC.dryTempVal = 0;
  ProgramC.preSpeedVal = 1;
  ProgramC.main1SpeedVal = 2;
  ProgramC.main2SpeedVal = 2;
  ProgramC.drySpeedVal = 3;
  ProgramC.main1Reps = 4;
  ProgramC.main2Reps = 4;
  ProgramC.dryReps = 3;
  ProgramC.progPrice = 510;

} 


struct instructions ProgramInstructions::GetProgramInstructions(uint8_t program)
{
  switch (program)
  {
    case 0:
      return ProgramA;
      break;
    case 1:
      return ProgramB;
      break;
    case 2:
      return ProgramC;
      break;
  }
}
