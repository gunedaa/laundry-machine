#ifndef IBUZZER
#define IBUZZER

#include <stdint.h>

class IBuzzer {

  public:
    virtual void SetBuzzer(uint8_t setValue) = 0;
    virtual unsigned long CurrentMillis() = 0;
    virtual void Delay(int duration) = 0;
};

#endif
