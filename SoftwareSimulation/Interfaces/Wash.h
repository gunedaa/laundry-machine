#ifndef WASH_H
#define WASH_H

#define MAIN_PRE_DURATION 10
#define DRYDURATION 5
#define CLOCKWISE 1
#define COUNTERCLOCKWISE 0

#include "Motor.h"
#include "Heater.h"
#include "Soap.h"
#include "Water.h"
#include "Door.h"
#include "Buzzer.h"
#include "ProgramInstructions.h"
#include "DirtSensor.h"
#include <stdint.h>

class Wash
{
  public:
    //Constructor
    Wash(Soap *sp, Water *water, Door *door, Buzzer *buzzer, Heater *heater, Motor *motor, ProgramInstructions *ProgramInstructions, uint8_t program, DirtSensor *dirtSensor);

    //This method executes the main process of Wash
    void DoWash();

    //This method checks if wash is done
    uint8_t IsWashDone();

  private:
    Heater *h;
    Motor *m;
    Soap *s;
    Water *w;
    Door *d;
    Buzzer *b;
    ProgramInstructions *pi;
    DirtSensor *ds;
    uint8_t p;

    int TemperaturePre = 0;
    int TemperatureMain = 0;
    int rounds = 0;
    int Reps = 0;
    int RepsDry = 0;

    /* This method handles the preWash state machine */
    void PreWashSM();

    /* This method handles the mainWash state machine */
    void MainWashSM();

    /* This method handles the centrifuge state machine */
    void CentrifugeSM();

    /* This method handles the state machine to be executed
      if the laundry machine doesnt have water pressure for 10 minutes */
    void QuitSM();

    /* This method handles what happens if there is or isnt water pressure
      and swithces the states accordingly */
    void WaterPressureHandler();


    uint8_t washIsDone = false;

    /* PREWASH: This state calls the PreWashSM() to execure the prewash instructions

       MAINWASH: This state calls the MainWashSM() to execure the mainwash instructions

       DRY: This state calls the CentrifugeSM() to execure the centrifuge instructions

       QUIT: This state calls the QuitSM() to exit the program
    */
    enum WashState {PREWASH, MAINWASH, DRY, QUIT};


    /*  WATERIN: This state handles the drain to let water in the tank

        USESOAP1: This state handles using soap from compartment 1

        USESOAP2: This state handles using softner from compartment 2

        HEAT: This state handles heating up the water

        STOPHEAT: This state handles turning the heater off

        ROTATEC: This state handles rotation of the motor in the clockwise direction

        ROTATECC: This state handles rotation of the motor in the counterclockwise direction

        WATEROUT: This state handles the sink to let the water out

        WAITING: This state handles the buzzer to indicate errors
    */
    enum WashStateProcess {WATERIN, USESOAP1, USESOAP2, HEAT, STOPHEAT, ROTATEC, ROTATECC, WATEROUT, WAITING};



    /*
       states declaration
    */
    WashState ws;
    WashState nextState;
    WashStateProcess wspPre;
    WashStateProcess lastWSPMain;
    WashStateProcess lastWSPPre;
    WashStateProcess wspMain;
    WashStateProcess wspDry;

};

#endif
