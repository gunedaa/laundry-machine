#include "Wash.h"
Wash::Wash(Soap *sp, Water *water, Door *door, Buzzer *buzzer, Heater *heater, Motor *motor, ProgramInstructions *ProgramInstructions, uint8_t program, DirtSensor *dirtSensor)
{
  s = sp;
  w = water;
  d = door;
  b = buzzer;
  h = heater;
  m = motor;
  pi = ProgramInstructions;
  p = program;
  ds = dirtSensor;
  ws = PREWASH;
  wspMain = WATERIN;
  wspPre = WATERIN;
  wspDry = WATEROUT;

}

void Wash::DoWash()
{
  switch (ws)
  {
    case PREWASH:
      this->PreWashSM();
      break;

    case MAINWASH:
      this->MainWashSM();
      break;

    case DRY:
      this->CentrifugeSM();
      break;

    case QUIT:
      QuitSM();
  }
}

uint8_t Wash::IsWashDone()
{
  return washIsDone;
}

void Wash::PreWashSM()
{
  h->SetHeaterLevel(TemperaturePre);

  if (wspPre == WATERIN || wspPre == WAITING)
  {
    this->WaterPressureHandler();
  }

  switch (wspPre)
  {

    case WATERIN:
      if (w->WaterIn((pi->GetProgramInstructions(p)).preWaterVal))
      {
        wspPre = USESOAP1;
      }
      break;

    case USESOAP1:

      switch ( ds->CheckDirtLevel(0) )
      {
        case 0:
          s->GiveMeSoap(1, 0);
          break;

        case 1:
          s->GiveMeSoap(1, 1);
          break;

        case 2:
          s->GiveMeSoap(1, 2);
          break;
      }

      wspPre = HEAT;
      break;

    case HEAT:

      TemperaturePre  = (pi->GetProgramInstructions(p)).preTempVal;

      if ( h->SetHeaterLevel(TemperaturePre))
      {
        m->Stopwatch();
        wspPre = ROTATEC;
      }
      break;


    case ROTATEC:

      if (m->Spin(CLOCKWISE, (pi->GetProgramInstructions(p)).preSpeedVal, MAIN_PRE_DURATION))
      {
        m->Stopwatch();
        wspPre = ROTATECC;
      }
      break;


    case ROTATECC:

      if (m->Spin(COUNTERCLOCKWISE, (pi->GetProgramInstructions(p)).preSpeedVal, MAIN_PRE_DURATION))
      {
        wspPre = STOPHEAT;
      }
      break;

    case STOPHEAT:
      TemperaturePre = 0;
      if (h->StopHeat())
      {
        wspPre = WATEROUT;
      }

      break;

    case WATEROUT:
      if (w->WaterOut())
      {
        ws = MAINWASH;
      }
      break;

    case WAITING:
      b->Buzz();
      break;
  }
}

void Wash::MainWashSM()
{
  h->SetHeaterLevel(TemperatureMain);

  if (wspMain == WATERIN || wspMain == WAITING)
  {
    this->WaterPressureHandler();
  }

  switch (wspMain)
  {
    case WATERIN:
      if (rounds == 0)
            {
              if ( w->WaterIn((pi->GetProgramInstructions(p)).main1WaterVal)  )
              {
                wspMain = USESOAP1;
              }
            }
            if (rounds == 1)
            {
              if ( w->WaterIn((pi->GetProgramInstructions(p)).main2WaterVal)  )
              {
                wspMain = USESOAP2;
              }
            }
      break;

    case USESOAP1:

      switch (ds->CheckDirtLevel(1)) {
        case 0:
          s->GiveMeSoap(1, 0);

          break;
        case 1:
          s->GiveMeSoap(1, 1);

          break;
        case 2:
          s->GiveMeSoap(1, 2);

          break;
        case 3:
          s->GiveMeSoap(1, 3);

          break;
      }

      wspMain = HEAT;
      break;

    case HEAT:
      TemperatureMain = (pi->GetProgramInstructions(p)).main1TempVal;
      if (h->SetHeaterLevel(TemperatureMain))
      {
        m->Stopwatch();
        wspMain = ROTATEC;
      }
      break;

    case STOPHEAT:

      TemperatureMain = 0;
      if (h->StopHeat())
      {
        wspMain = WATEROUT;
      }

      break;

    case USESOAP2:
      s->GiveMeSoap(2, 1);
      m->Stopwatch();
      wspMain = ROTATEC;

      break;

    case ROTATEC:

      if (m->Spin(CLOCKWISE, (pi->GetProgramInstructions(p)).main2SpeedVal, MAIN_PRE_DURATION))
      {
        m->Stopwatch();
        wspMain = ROTATECC;
      }
      break;

    case ROTATECC:

      if (m->Spin(COUNTERCLOCKWISE, (pi->GetProgramInstructions(p)).main2SpeedVal, MAIN_PRE_DURATION))
      {
        Reps++;
        if (Reps < (pi->GetProgramInstructions(p)).main1Reps) {
          m->Stopwatch();
          wspMain = ROTATEC;
        }
        if (Reps == (pi->GetProgramInstructions(p)).main1Reps) {
          wspMain = STOPHEAT;

        }
      }
      break;

    case WATEROUT:
      if (w->WaterOut())
      {
        if (rounds == 1)
        {
          ws = DRY;
        }
        if (rounds == 0)
        {
          rounds++;
          Reps = 0;
          wspMain = WATERIN;
        }
      }
      break;

    case WAITING:
      b->Buzz();
      break;
  }
}

void Wash::CentrifugeSM()
{
  switch (wspDry)
  {
    case ROTATEC:
      w->WaterOut();

      if (m->Spin(CLOCKWISE, (pi->GetProgramInstructions(p)).drySpeedVal, DRYDURATION))
      {
        wspDry = ROTATECC;
        m->Stopwatch();
      }
      break;

    case ROTATECC:
      w->WaterOut();

      if ( m->Spin(COUNTERCLOCKWISE, (pi->GetProgramInstructions(p)).drySpeedVal, DRYDURATION))
      {
        RepsDry++;
        if (RepsDry < (pi->GetProgramInstructions(p)).dryReps)
        {
          m->Stopwatch();
          wspDry = ROTATEC;

        }

        if (RepsDry == (pi->GetProgramInstructions(p)).dryReps)
        {
          wspDry = WATEROUT;
        }
      }
      break;

    case WATEROUT:
      if ( w->WaterOut())
      {
        m->Stopwatch();
        wspDry = ROTATEC;
        if (RepsDry == (pi->GetProgramInstructions(p)).dryReps)
        {
          ds->CheckDirtLevel(3);
          washIsDone = true;
          d->LockInternally(0);
        }
      }
      break;
  }
}

void Wash::QuitSM()
{
//  wspDry = WATEROUT;
//  wspMain = WAITING;

  if (w->WaterOut())
  {
    ds->CheckDirtLevel(3);
    washIsDone = true;
    d->LockInternally(0);
  }
}

void Wash::WaterPressureHandler()
{

  switch (w->PressureCheck())
  {
    case -1:
      ws = QUIT;

      break;
    case 0:
      if (wspMain != WAITING || wspPre != WAITING)
      {
        w->CloseWaterIn();
        lastWSPMain = wspMain;
        lastWSPPre = wspPre;
        wspMain = WAITING;
        wspPre = WAITING;
      }
      break;
    case 1:
      if (wspMain == WAITING || wspPre == WAITING)
      {
        wspMain = lastWSPMain;
        wspPre = lastWSPPre;
      }
      break;
  }
}
