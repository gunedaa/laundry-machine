#include "ProgramSelector.h"


ProgramSelector::ProgramSelector(IProgram *prog)
{
  p = prog;
  currentProgram = 2;
  lastBtnStart = false;
  currentBtnStart = false;
  lastBtnProg = false;
  currentBtnProg = false;
}

uint8_t ProgramSelector::IsStartButtonPressed()
{
  return p->GetBtnStart();
}

void ProgramSelector::CheckProgramInput()
{
  currentBtnProg = p->GetBtnProgram();
  if (currentBtnProg && lastBtnProg == false)
  {
    if (currentProgram < 2)
    {
      lastBtnProg = currentBtnProg;
      currentProgram++;
      p->SetProgram(currentProgram);
    }
    else {
      currentProgram = 0;
      p->SetProgram(currentProgram);
    }
  }
  if (currentBtnProg == 0 && lastBtnProg == true)
  {
    lastBtnProg = false;
  }
}

ProgramOptions ProgramSelector::GetCurrentProgram()
{
  return (ProgramOptions)currentProgram;

}
