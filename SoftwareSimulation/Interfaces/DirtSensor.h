#ifndef DIRTSENSOR_H
#define DIRTSENSOR_H

#include <stdint.h>

#include "IDirtSensor.h"

class DirtSensor
{
  public:
    DirtSensor(IDirtSensor *dirtsensor);
    uint8_t CheckDirtLevel(uint8_t washType);

  private:
    IDirtSensor *dirtSensor;

};

#endif
