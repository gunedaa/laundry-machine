#ifndef WATER_H
#define WATER_H

#define QUITINTERVAL 10

#include "IWater.h"
#include <stdint.h>

class Water
{
  public:
    //constructor
    Water(IWater *water);

    //This method return wether there is water pressure or not
    uint8_t IsThereWaterPressure();

    //this method handles filling the water tank to given level
    bool WaterIn(uint8_t level);

    //this methods handles the sink to let water out
    bool WaterOut();

    //this method closes the drain
    void CloseWaterIn();

    //this method return the current water level
    uint8_t CheckWaterLevel();

    //this method handles what happens to the process if water pressure drops
    int PressureCheck();

    //this method return the current millis
    unsigned long CurrentMillis();

    //this method saves the returned millis
    void Stopwatch();


  private:
    IWater *w;
    unsigned long PrevTime = 0;
    int previousCheckResponse = 1;





};

#endif
