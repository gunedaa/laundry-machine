#ifndef IHEATER
#define IHEATER

#include <stdint.h>

class IHeater {

  public:
    virtual void SetHeaterStatus(uint8_t setValue) = 0;
    virtual uint8_t GetCurrentTemperatureLevel() = 0;
};

#endif
