#ifndef DOOR_H
#define DOOR_H

#include "IDoor.h"

class Door
{
  public:
    Door(IDoor *door);
    void LockInternally(uint8_t state);
    uint8_t CheckExternalDoorState();

  private:
    IDoor *d;

};

#endif
