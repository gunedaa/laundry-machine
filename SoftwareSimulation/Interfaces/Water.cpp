#include "Water.h"

Water::Water(IWater *water)
{
  w = water;
}

uint8_t Water::IsThereWaterPressure()
{
  return w->GetWaterPressure();

}

bool Water::WaterIn(uint8_t level)
{
  bool response = false;

  if (w->GetWaterLevel() < level)
  {
    w->SetSink(0);
    w->SetDrain(1);
  }

  if (w->GetWaterLevel() == level)
  {
    response = true;
    w->SetDrain(0);
  }
  return response;

}
bool Water::WaterOut()
{
  bool response = false;
  if (w->GetWaterLevel() > 0)
  {
    w->SetDrain(0);
    w->SetSink(1);
  }
  if (w->GetWaterLevel() == 0)
  {
    response = true;
    w->SetSink(0);
  }

  return response;
}

uint8_t Water::CheckWaterLevel()
{
  //Serial.println( w->GetWaterLevel());
  return w->GetWaterLevel();

}

int Water::PressureCheck()
{
  int response = 1;

  if (w->GetWaterPressure())
  {
    response = 1;
  }
  else
  {
    response = 0;
    if (previousCheckResponse != response)
    {
      this->Stopwatch();
    }

    unsigned long passedTime = ((w->CurrentMillis() - PrevTime) / 1000);

    if (passedTime > QUITINTERVAL)
    {
      
      response = -1;
    }
  }
  previousCheckResponse = response;
  return response;
}

void Water::Stopwatch()
{
  PrevTime = w->CurrentMillis();
}

void Water::CloseWaterIn()
{
   w->SetDrain(0);
}
