QT       += core gui
QT += multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    ../Interfaces/Buzzer.cpp \
    ../Interfaces/Coins.cpp \
    ../Interfaces/Director.cpp \
    ../Interfaces/DirtSensor.cpp \
    ../Interfaces/Door.cpp \
    ../Interfaces/Heater.cpp \
    ../Interfaces/Motor.cpp \
    ../Interfaces/ProgramInstructions.cpp \
    ../Interfaces/ProgramSelector.cpp \
    ../Interfaces/Soap.cpp \
    ../Interfaces/Wash.cpp \
    ../Interfaces/Water.cpp \
    main.cpp \
    simulation.cpp

HEADERS += \
    ../Interfaces/Buzzer.h \
    ../Interfaces/Coins.h \
    ../Interfaces/Director.h \
    ../Interfaces/DirtSensor.h \
    ../Interfaces/Door.h \
    ../Interfaces/Heater.h \
    ../Interfaces/IBuzzer.h \
    ../Interfaces/ICoins.h \
    ../Interfaces/IDirtSensor.h \
    ../Interfaces/IDoor.h \
    ../Interfaces/IHeater.h \
    ../Interfaces/IMotor.h \
    ../Interfaces/IProgram.h \
    ../Interfaces/ISoap.h \
    ../Interfaces/IWater.h \
    ../Interfaces/Motor.h \
    ../Interfaces/ProgramInstructions.h \
    ../Interfaces/ProgramSelector.h \
    ../Interfaces/Soap.h \
    ../Interfaces/Wash.h \
    ../Interfaces/Water.h \
    simulation.h

FORMS += \
    simulation.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
