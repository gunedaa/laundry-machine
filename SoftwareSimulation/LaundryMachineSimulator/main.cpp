#include "simulation.h"
#include "ui_simulation.h"
#include <QApplication>

Simulation *simulation;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    simulation = new Simulation();

    simulation->show();
    return a.exec();
}
