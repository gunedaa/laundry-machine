#ifndef SIMULATION_H
#define SIMULATION_H

#include <QMainWindow>
#include "../Interfaces/ICoins.h"
#include "../Interfaces/IProgram.h"
#include "../Interfaces/ISoap.h"
#include "../Interfaces/Director.h"
#include "../Interfaces/IDoor.h"
#include "../Interfaces/IWater.h"
#include "../Interfaces/IMotor.h"
#include "../Interfaces/IBuzzer.h"
#include "../Interfaces/IHeater.h"
#include "../Interfaces/IDirtSensor.h"
#include <QBasicTimer>
#include <QtMultimedia/QMediaPlayer>
#include <QWidget>

QT_BEGIN_NAMESPACE
namespace Ui { class Simulation; }
QT_END_NAMESPACE

class Simulation : public QMainWindow, public ICoins, public IProgram, public ISoap, public IDoor, public IWater, public IMotor, public IBuzzer
        ,public IHeater, public IDirtSensor
{
    Q_OBJECT
public:
    /* Constructor */
    Simulation(QWidget *parent = nullptr);

    /* Desctructor */
    ~Simulation();

    //------------------------------------------------Coins------------------------------------------------------------v
    /* Gets the status of coin btn 10
         * true -> pressed
         * false -> not pressed
     */
    uint8_t GetBtn_10();

    /* Gets the status of coin btn 50
         * true -> pressed
         * false -> not pressed
     */
    uint8_t GetBtn_50();

    /* Gets the status of coin btn 200
         * true -> pressed
         * false -> not pressed
     */
    uint8_t GetBtn_200();

    /* Sets the coin 10 checkboxes
     */
    void SetCoin_10(uint8_t nrOfCoins);

    /* Sets the coin 50 checkboxes
     */
    void SetCoin_50(uint8_t nrOfCoins);

    /* Sets the coin 200 checkboxes
     */
    void SetCoin_200(uint8_t nrOfCoins);

    /* Gets the status of clear btn
         * true -> pressed
         * false -> not pressed
     */
    uint8_t GetBtnClearCoins();

    /* Clears all the coins
     */
    void ClearCoins();

    //------------------------------------------------Soap-------------------------------------------------------------v
    /* Gets the status of soap 1 btn
         * true -> pressed
         * false -> not pressed
     */
    uint8_t GetSwitchSoap1();

    /* Gets the status of soap 2 btn
         * true -> pressed
         * false -> not pressed
     */
    uint8_t GetSwitchSoap2();

    /* Sets the soap 1 checkbox
     */
    void SetSoap1(uint8_t setVal);

    /* Sets the soap 2 checkbox
     */
    void SetSoap2(uint8_t setVal);


    //---------------------------------------------Program-------------------------------------------------------------v

    /* Gets the status of program btn
         * true -> pressed
         * false -> not pressed
     */
    uint8_t GetBtnProgram();

    /* Gets the status of start btn
         * true -> pressed
         * false -> not pressed
     */
    uint8_t GetBtnStart();

    /* Sets the program radiobuttons
     */
    void SetProgram(uint8_t setVal);

    //-----------------------------------------------Door---------------------------------------------------------------v
    /*Sets the internal Lock
        * setValue = 1 -> locked
        * setValue = 0 -> unlocked
     */
    void SetInternalLock(uint8_t setValue);

    /*retunrs if the door is closed
        * true  -> closed
        * false -> opened
     */
    uint8_t GetSwitchDoorLock();

    //-----------------------------------------------Water--------------------------------------------------------------v
    /*Sets the drain state
        * setVal = 1 -> drain opened
        * setVal = 0 -> drain closed
     */
     void SetDrain(uint8_t setVal);

     /*Sets the sink state
        * setVal = 1 -> sink opened
        * setVal = 0 -> sink closed
      */
     void SetSink(uint8_t setVal);

     /*Returns the current water level
        * 0 -> no water
        * 1 -> first level
        * 2 -> second level
        * 3 -> third level
      */
     uint8_t GetWaterLevel();

     /*Returns the current water pressure state
        * 0 -> no water pressure
        * 1 -> water pressure detected
      */
     uint8_t GetWaterPressure();

     /*Returns the current time since the program started in milliseconds
      */
     unsigned long CurrentMillis();

    //-----------------------------------------------Motor---------------------------------------------------------------v

     /*Sets the speed for motor
        * 0 -> motor off
        * 1 -> slow speed
        * 2 -> medium speed
        * 3 -> high speed
      */
     void SetSpeed(int level);

     /*Sets the rotation direction for the motor
      * 0 -> right
      * 1 -> left
      */
     void SetRotation(int direction);

     /*Sets the duration to pass in seconds
      */
     uint8_t SetDuration(long duration);

     /*Returns the current rotation
      * 0 -> right
      * 1 -> left
      */
     uint8_t CurrentRotation();

     /*Returns the current rotation
        * 0 -> motor off
        * 1 -> slow speed
        * 2 -> medium speed
        * 3 -> high speed
      */
     uint8_t CurrentSpeed();

    //-------------------------------------------------Buzzer-------------------------------------------------------------v

     /*Sets the buzzer on
     * 1 -> buzz
     */
    void SetBuzzer(uint8_t setVal);

    /*Non functional in the simulation
     */
    void Delay(int duration);

    //-------------------------------------------------Heater-------------------------------------------------------------v

    /*Sets the Heater
        * 0 -> Heater on
        * 1 -> Heater off
     */
     void SetHeaterStatus(uint8_t setValue);

     /*Returns the current temperature level
        * 0 -> no heat
        * 1 -> first level
        * 2 -> second level
        * 3 -> third level
      */
     uint8_t GetCurrentTemperatureLevel();

    //------------------------------------------------DirtSensor----------------------------------------------------------v

    /*Returns the dirt level of the clothes
        * 0 -> no dirt
        * 1 -> first level
        * 2 -> second level
        * 3 -> third level
     */
    uint8_t GetDirtLevel(uint8_t washType);


    //------------------------------------------------Additional----------------------------------------------------------v
     void PressureAnimation(int value);

private:
    Ui::Simulation *ui;
    Coins *coins;
    Director *director;
    Soap *sp;
    ProgramSelector *ps;
    Door *dr;
    Water *wt;
    Motor *mt;
    Buzzer *bz;
    Heater *ht;
    DirtSensor *ds;
    ProgramInstructions pi;

    QTimer *timer;
    QTimer *timerWash;
    QTimer *timerRotation;
    QTimer *timerBehaviour;
    QTimer *timerAnimate;
    QTimer *SoftwareTimer;

    QMediaPlayer *player;

    bool button10;
    bool button200;
    bool button50;
    bool buttonCLR;
    bool buttonSoap1;
    bool buttonSoap2;
    bool onOff;
    bool program;
    bool buttonStart;
    bool buttonDoor;
    bool internalLock;
    bool waterPressure;
    bool drainState;
    int drainCounter;
    bool sinkState;
    int sinkCounter;
    int motorSpeed;
    bool motorDirection;
    int motorCounter;
    int animation;
    uint8_t tempLevel;
    bool heater;
    int heaterCounter;
    int waterCounter;
    uint8_t dirtLevel;
    long int millis;
    long int currentMillis;
    int assignment;
    int started;
    int reached;
    int speedChange;


private slots:
    /* Coin 10 btn event
     * Changes coin 10 btn state to true when it is clicked
     */
    void on_pushButton_10_clicked();

    /* Coin 50 btn event
     * Changes coin 50 btn state to true when it is clicked
     */
    void on_pushButton_50_clicked();

    /* Coin 200 btn event
     * Changes coin 200 btn state to true when it is clicked
     */
    void on_pushButton_200_clicked();

    /* Clear btn event
     * Changes Clear btn state to true when it is clicked
     */
    void on_pushButton_CLR_clicked();

    /* Soap 1 btn event
     * Changes Soap 1 btn state when it is clicked
         * to true if it was false
         * to false if it was true
     */
    void on_pushButton_8_clicked();

    /* Soap 2 btn event
     * Changes Soap 2 btn state when it is clicked
         * to true if it was false
         * to false if it was true
     */
    void on_pushButton_9_clicked();

    /* Start btn event
     * Changes Start btn state to true when it is clicked
     */
    void on_pushButton_11_clicked();

    /* Program btn event
     * Changes Program btn state to true when it is clicked
     */
    void on_pushButton_12_clicked();

    /* On/Off btn event
     * Changes On/Off btn state when it is clicked
         * to true if it was false
         * to false if it was true
     */
    void on_pushButton_clicked();

    void on_pushButton_7_clicked();
    void on_pushButton_2_clicked();

    /* The function which will be raised every time the first timer ticks
        * Calls the director Work() function
        */

    //----------------------------------------------------Timer Functions-----------------------------------------v
    void Animate();
    void WaterBehaviour();
    void TimerDrainSink();
    void HeaterBehaviour();
    void TimerRotation();
    void IntelliganceWork();
    void ResetStatusOfButtons();
    void Rotate(int led);





};
#endif // SIMULATION_H
