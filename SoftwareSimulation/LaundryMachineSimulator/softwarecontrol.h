#ifndef SOFTWARECONTROL_H
#define SOFTWARECONTROL_H
#include <QMainWindow>
#include "simulation.h"
#include "ui_simulation.h"

class SoftwareControl
{
public:
    Ui::Simulation * ui;
    SoftwareControl();
    void Drain(int val);
    void Sink(int val);
private slots:
    void on_pushButton_11_clicked();
};

#endif // SOFTWARECONTROL_H
