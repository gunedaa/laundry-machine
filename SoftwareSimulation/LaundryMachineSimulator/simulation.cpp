#include "simulation.h"
#include "ui_simulation.h"
#include <QTimer>
#include <QDebug>
#include <QRandomGenerator>
#include <QTime>
#include <QApplication>


Simulation::Simulation(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::Simulation)
{
    //----------------Global Variables---------------
    internalLock = false;   //lock handler var
    button10 = false;       //button 10 handler var
    button50 = false;       //button 50 handler var
    button200 = false;      //button 200 handler var
    buttonCLR = false;      //button clear handler var
    buttonSoap1 = false;    //button soap1 handler var
    buttonSoap2 = false;    //button soap2 handler var
    buttonDoor = false;     //button door handler var
    drainState = false;     //drin button state handler var
    sinkState = false;      //sink button state handler var
    onOff = false;          //on/off button handler var
    program = true;         //program button handler var
    buttonStart = false;    //start button handler var
    waterPressure = false;  //pressure handler
    drainCounter = 1;       //used to handle drain LEDs in the TimerDrainSink
    sinkCounter =1;         //used to handle sink LEDs in the TimerDrainSink
    motorSpeed = 0;         //current motor speed
    motorDirection= 0;      // 0 = rigt and 1 = left motor direction
    motorCounter = 1;       //used to animate the motor rotation
    animation = 0;          //water pressure animation variable
    tempLevel = 0;          //current temperature level
    heater = 0;             //heater state (bool)
    heaterCounter = 0;      //handles the heater animation in HeaterBehaviour
    waterCounter = 0;       //handles the water filling animation in the WaterBehaviour
    dirtLevel = 0;          //randomly generated dirt level
    millis = 0;             //current milliseconds from the start of the program
    currentMillis = 0;      //used in SetDuration to calculate the time passed
    assignment = 0;         //used in SetDuration to indicate that the motor needs to finish the assigned rotation and only after accept new commands
    started = 0;            //handles heater
    speedChange = 0;        //used in SetSpeed to keep the intelegence constantly setting the speed, instead changes speed only on change

    //----------------UI setup--------------
    ui->setupUi(this); //innitiates the UI
    this->setFixedSize(709, 626); //fixed size for UI


    //----------------Buzzer setup-------------
    player = new QMediaPlayer;
    player->setMedia(QUrl::fromLocalFile("../Sounds/warning2.mp3"));
    player->setVolume(30);


    //----------------Timer Declaration--------------------

    timer = new QTimer(this);
    timerWash = new QTimer(this);
    timerRotation= new QTimer(this);
    timerBehaviour=new QTimer(this);
    timerAnimate = new QTimer(this);

    //-----------------Connecting timer to a function -----------------------------------------------------------------------------------------------------------

    connect(timer, &QTimer::timeout, this, &Simulation::IntelliganceWork );         //Executes Work() and counts milliseconds
    connect(timerRotation, &QTimer::timeout, this,&Simulation::TimerRotation);      //Handles Motor animation and speed
    connect(timerWash, &QTimer::timeout, this, &Simulation::TimerDrainSink);        //Handles drain and sink animation
    connect(timer, &QTimer::timeout, this, &Simulation::ResetStatusOfButtons );     //Resets the buttons
    connect(timerAnimate, &QTimer::timeout, this,&Simulation::Animate);             //Responsible for Pressure and dirt sensor animations
    connect(timerBehaviour, &QTimer::timeout, this,&Simulation::HeaterBehaviour);   //Handles the Heater
    connect(timerBehaviour, &QTimer::timeout, this,&Simulation::WaterBehaviour);    //Handles the Water


    //--------------Timers Start------------------------------------------------------------------------------------------------------
    timer->start(1);                // timer responsible for executing the Work() method and keeping track of the milliseconds passed
    timerWash->start(750);          //750 ms
    timerAnimate->start(1000);      //1s
    timerBehaviour->start(4000);    //4s


    //------------------Object Setup---------------------------------------------
    coins = new Coins(this);        //Coins
    ps = new ProgramSelector(this); //ProgramSelector
    sp = new Soap(this);            //Soap
    dr = new Door(this);            //Door
    wt = new Water(this);           //Water
    mt = new Motor(this);           //Motor
    bz = new Buzzer(this);          //Buzzer
    ht = new Heater(this);          //Heater
    pi =  ProgramInstructions();    //ProgramInstructions
    ds =  new DirtSensor(this);     //DirtSensor
    director = new Director(coins,ps,&pi,sp,wt,dr,bz,ds,mt,ht); //Director


    //there is no other way of setting the UI objects invisible
    //----------------Image setup--------------------------------
    ui->coins10LED1->setVisible(0);
    ui->coins10LED2->setVisible(0);
    ui->coins10LED3->setVisible(0);
    ui->coins50LED1->setVisible(0);
    ui->coins50LED2->setVisible(0);
    ui->coins50LED3->setVisible(0);
    ui->coins200LED1->setVisible(0);
    ui->coins200LED2->setVisible(0);
    ui->locked->setVisible(0);
    ui->soap2LED->setVisible(0);
    ui->pressure1LED->setVisible(0);
    ui->programLEDA->setVisible(1);
    ui->programLEDB->setVisible(0);
    ui->programLEDC->setVisible(0);
    ui->LED_Heater->setVisible(0);
    ui->LED_Water_1->setVisible(0);
    ui->LED_Water_2->setVisible(0);
    ui->LED_Water_3->setVisible(0);
    ui->LED_Temp_1->setVisible(0);
    ui->LED_Temp_2->setVisible(0);
    ui->LED_Temp_3->setVisible(0);
    ui->LED_Wash_1->setVisible(0);
    ui->LED_Wash_2->setVisible(0);
    ui->LED_Wash_3->setVisible(0);
    ui->LED_Wash_4->setVisible(0);
    ui->LED_Wash_5->setVisible(0);
    ui->LED_Wash_6->setVisible(0);
    ui->LED_Wash_7->setVisible(0);
    ui->LED_Wash_8->setVisible(0);
    ui->LED_Wash_9->setVisible(0);
    ui->LED_Wash_10->setVisible(0);
    ui->LED_Wash_11->setVisible(0);
    ui->LED_Wash_12->setVisible(0);
    ui->LED_Wash_13->setVisible(0);
    ui->LED_Wash_14->setVisible(0);
    ui->LED_Wash_15->setVisible(0);
    ui->LED_Wash_16->setVisible(0);
    ui->LED_Wash_17->setVisible(0);
    ui->LED_Wash_18->setVisible(0);
    ui->LED_Wash_19->setVisible(0);
    ui->LED_Wash_20->setVisible(0);
    ui->LED_Wash_21->setVisible(0);
    ui->LED_Wash_22->setVisible(0);
    ui->pressure05LED->setVisible(0);
    ui->LED_dirtSensor1->setVisible(0);
    ui->LED_dirtSensor2->setVisible(0);
    ui->LED_dirtSensor3->setVisible(0);



    //---------------Door and Soap interactive Button setup-------------------
    ui->pushButton_8->setIcon(QIcon("../Images/fsg"));
    ui->pushButton_8->setIconSize(QSize(50,50));
    ui->pushButton_9->setIcon(QIcon("../Images/fsg"));
    ui->pushButton_9->setIconSize(QSize(50,50));
    ui->pushButton_7->setIcon(QIcon("../Images/fsg"));
    ui->pushButton_7->setIconSize(QSize(50,50));


}

//-----------------------------Timer Functions------------------------------------------

//Executes Work() and counts milliseconds
void Simulation::IntelliganceWork()
{
    director->Work();
    millis++;

}

//Handles Motor animation and speed
void Simulation::TimerRotation(){
    if(motorSpeed>0){
        if (motorDirection ==0){
            Rotate(motorCounter);
            motorCounter++;

            if (motorCounter == 13){
                motorCounter=1;
            }
        }
        else if (motorDirection ==1){
            if (motorCounter == 0){
                motorCounter=13;
            }
            Rotate(motorCounter);
            motorCounter--;
        }
    }
}

//Handles drain and sink animation
void Simulation::TimerDrainSink(){
    if(drainState){
        if (drainCounter==1){
            ui->LED_Wash_1->setVisible(1);
            ui->LED_Wash_2->setVisible(0);
            ui->LED_Wash_3->setVisible(0);
            ui->LED_Wash_4->setVisible(0);
            ui->LED_Wash_5->setVisible(0);
            drainCounter++;
        }
        else if (drainCounter==2){
            ui->LED_Wash_1->setVisible(0);
            ui->LED_Wash_2->setVisible(1);
            ui->LED_Wash_3->setVisible(0);
            ui->LED_Wash_4->setVisible(0);
            ui->LED_Wash_5->setVisible(0);
            drainCounter++;
        }
        else if (drainCounter==3){
            ui->LED_Wash_1->setVisible(0);
            ui->LED_Wash_2->setVisible(0);
            ui->LED_Wash_3->setVisible(1);
            ui->LED_Wash_4->setVisible(0);
            ui->LED_Wash_5->setVisible(0);
            drainCounter++;
        }
        else if (drainCounter==4){
            ui->LED_Wash_1->setVisible(0);
            ui->LED_Wash_2->setVisible(0);
            ui->LED_Wash_3->setVisible(0);
            ui->LED_Wash_4->setVisible(1);
            ui->LED_Wash_5->setVisible(0);
            drainCounter++;
        }
        else if (drainCounter==5){
            ui->LED_Wash_1->setVisible(0);
            ui->LED_Wash_2->setVisible(0);
            ui->LED_Wash_3->setVisible(0);
            ui->LED_Wash_4->setVisible(0);
            ui->LED_Wash_5->setVisible(1);
            drainCounter = 1;
        }
    }
    else{
        ui->LED_Wash_1->setVisible(0);
        ui->LED_Wash_2->setVisible(0);
        ui->LED_Wash_3->setVisible(0);
        ui->LED_Wash_4->setVisible(0);
        ui->LED_Wash_5->setVisible(0);
    }
    if(sinkState){
        if (sinkCounter==1){
            ui->LED_Wash_18->setVisible(1);
            ui->LED_Wash_19->setVisible(0);
            ui->LED_Wash_20->setVisible(0);
            ui->LED_Wash_21->setVisible(0);
            ui->LED_Wash_22->setVisible(0);
            sinkCounter++;
        }
        else if (sinkCounter==2){
            ui->LED_Wash_18->setVisible(0);
            ui->LED_Wash_19->setVisible(1);
            ui->LED_Wash_20->setVisible(0);
            ui->LED_Wash_21->setVisible(0);
            ui->LED_Wash_22->setVisible(0);
            sinkCounter++;
        }
        else if (sinkCounter==3){
            ui->LED_Wash_18->setVisible(0);
            ui->LED_Wash_19->setVisible(0);
            ui->LED_Wash_20->setVisible(1);
            ui->LED_Wash_21->setVisible(0);
            ui->LED_Wash_22->setVisible(0);
            sinkCounter++;
        }
        else if (sinkCounter==4){
            ui->LED_Wash_18->setVisible(0);
            ui->LED_Wash_19->setVisible(0);
            ui->LED_Wash_20->setVisible(0);
            ui->LED_Wash_21->setVisible(1);
            ui->LED_Wash_22->setVisible(0);
            sinkCounter++;
        }
        else if (sinkCounter==5){
            ui->LED_Wash_18->setVisible(0);
            ui->LED_Wash_19->setVisible(0);
            ui->LED_Wash_20->setVisible(0);
            ui->LED_Wash_21->setVisible(0);
            ui->LED_Wash_22->setVisible(1);
            sinkCounter = 1;
        }
    }else{
        ui->LED_Wash_18->setVisible(0);
        ui->LED_Wash_19->setVisible(0);
        ui->LED_Wash_20->setVisible(0);
        ui->LED_Wash_21->setVisible(0);
        ui->LED_Wash_22->setVisible(0);
    }


}

//Resets the buttons
void Simulation::ResetStatusOfButtons()
{
    /* reset the button states before executing any function */
    button10 = false;
    button50 = false;
    button200 = false;
    buttonCLR = false;
    buttonSoap1 = false;
    buttonSoap2 = false;
    program = false;
    buttonStart = false;
}

//Responsible for Pressure and dirt sensor animations
void Simulation::Animate(){
    //Pressure Handle
    if(animation==1){
        animation++;
        PressureAnimation(1);
    }else if (animation == 2){
        PressureAnimation(0);
        animation=0;
    }
    else if (animation ==3){
        PressureAnimation(1);
        animation++;
    }else if (animation ==4){
        PressureAnimation(2);
        animation =0;
    }

    //Dirt Sensor Handle
    if(dirtLevel ==0){
        ui->LED_dirtSensor1->setVisible(0);
        ui->LED_dirtSensor2->setVisible(0);
        ui->LED_dirtSensor3->setVisible(0);

    }else if (dirtLevel ==1){
        ui->LED_dirtSensor1->setVisible(1);
        ui->LED_dirtSensor2->setVisible(0);
        ui->LED_dirtSensor3->setVisible(0);
    }else if (dirtLevel ==2){
        ui->LED_dirtSensor1->setVisible(1);
        ui->LED_dirtSensor2->setVisible(1);
        ui->LED_dirtSensor3->setVisible(0);
    }else if(dirtLevel ==3){
        ui->LED_dirtSensor1->setVisible(1);
        ui->LED_dirtSensor2->setVisible(1);
        ui->LED_dirtSensor3->setVisible(1);
    }

}

//Handles the Heater
void Simulation::HeaterBehaviour(){
    if(started){
        if (heater){
            if(heaterCounter==0){
                ui->LED_Temp_1->setVisible(0);
                ui->LED_Temp_2->setVisible(0);
                ui->LED_Temp_3->setVisible(0);
                heaterCounter++;
            }else if (heaterCounter==1){

                ui->LED_Temp_1->setVisible(1);
                heaterCounter++;


            }else if(heaterCounter==2){
                ui->LED_Temp_2->setVisible(1);
                heaterCounter++;


            }else if (heaterCounter==3){
                ui->LED_Temp_3->setVisible(1);
                heaterCounter++;

            }
            else if(heaterCounter==4){
                heaterCounter=3;
            }

        }
        else if (!heater){

            if(heaterCounter==1){
                ui->LED_Temp_2->setVisible(0);
                ui->LED_Temp_3->setVisible(0);
                heaterCounter--;

            }else if (heaterCounter==2){
                ui->LED_Temp_3->setVisible(0);
                heaterCounter--;

            }else if(heaterCounter==3){
                heaterCounter--;


            }
            else  if(heaterCounter==0){
                ui->LED_Temp_1->setVisible(0);
                ui->LED_Temp_2->setVisible(0);
                ui->LED_Temp_3->setVisible(0);
            }
            else if (heaterCounter==4){
                heaterCounter=3;
            }
        }

    }

}

//Handles the Water
void Simulation::WaterBehaviour(){
    if(drainState){
        if(waterCounter==0){
            waterCounter = -1;
        }else if (waterCounter==1){
            ui->LED_Water_1->setVisible(1);
            waterCounter++;
        }else if(waterCounter==2){
            ui->LED_Water_2->setVisible(1);
            waterCounter++;
        }else if (waterCounter==3){
            ui->LED_Water_3->setVisible(1);
            waterCounter++;
        }
        else if(waterCounter==4){
            waterCounter = 3;
        }
        else if (waterCounter==-1){
            waterCounter=1;
        }

    }
    else if (sinkState){
        if(waterCounter==1){
            ui->LED_Water_1->setVisible(0);
            waterCounter--;
        }else if (waterCounter==2){
            ui->LED_Water_2->setVisible(0);
            waterCounter--;
        }else if(waterCounter==3){
            ui->LED_Water_3->setVisible(0);
            waterCounter--;
        }
        else  if(waterCounter==0){
            ui->LED_Water_1->setVisible(0);
            ui->LED_Water_2->setVisible(0);
            ui->LED_Water_3->setVisible(0);

        }

        else if(waterCounter==4){
            waterCounter = 3;
        }

        else if (waterCounter==-1){
            waterCounter=1;
        }
    }

}


Simulation::~Simulation()
{
    delete ui;
}

//animation for the pressure dial
void Simulation::PressureAnimation(int value){
    if (value==1){
        ui->pressure05LED->setVisible(1);
        ui->pressure0LED->setVisible(0);
        ui->pressure1LED->setVisible(0);
    }
    else if (value==0){
        ui->pressure05LED->setVisible(0);
        ui->pressure0LED->setVisible(1);
        ui->pressure1LED->setVisible(0);

    }
    else if (value ==2){
        ui->pressure05LED->setVisible(0);
        ui->pressure0LED->setVisible(0);
        ui->pressure1LED->setVisible(1);

    }

}

//-------------Events-------------------------------------

//button 10
void Simulation::on_pushButton_10_clicked()
{
    button10 = true;
}

//button 50
void Simulation::on_pushButton_50_clicked()
{
    button50 = true;
}

//button 200
void Simulation::on_pushButton_200_clicked()
{
    button200 = true;
}

//clear button
void Simulation::on_pushButton_CLR_clicked()
{
    buttonCLR = true;
}

//soap1 button
void Simulation::on_pushButton_8_clicked()
{
    buttonSoap1 =true;
}

//soap2 button
void Simulation::on_pushButton_9_clicked()
{
    buttonSoap2 =true;
    ui->pushButton_9->setIcon(QIcon("../Images/lol"));
    ui->pushButton_9->setIconSize(QSize(50,50));
}

//start button
void Simulation::on_pushButton_11_clicked()
{
    buttonStart = true;
}

//program button
void Simulation::on_pushButton_12_clicked()
{
    program = true;
}

//-ON/-OFF button
void Simulation::on_pushButton_clicked()
{
    if(onOff == false){
        onOff = true;
        ui->groupBox_4->setChecked(1);
        ui->groupBox_3->setChecked(1);
        ui->groupBox_2->setChecked(1);
        ui->groupBox->setChecked(1);
    }
    else{
        onOff = false;
        ui->groupBox_4->setChecked(0);
        ui->groupBox_3->setChecked(0);
        ui->groupBox_2->setChecked(0);
        ui->groupBox->setChecked(0);
    }
}


//plays the buzz sound
void Simulation::SetBuzzer(uint8_t setVal)
{
    if(setVal == 1){
        player->play();
    }
}

void Simulation::Delay(int duration)
{
    //not in use
}

//set the heater state
void Simulation::SetHeaterStatus(uint8_t setValue)
{
    heater = setValue;
    if (heater==1){
        started = 1;
        ui->LED_Heater->setVisible(1);
    }
    else if (heater==0) {
        ui->LED_Heater->setVisible(0);
    }
}

//returns the current temperature
uint8_t Simulation::GetCurrentTemperatureLevel()
{
    int temp = 1;
    if(heater){
        temp=heaterCounter-1;
    }
    else if(!heater){
        if(heaterCounter > 0){
            temp =heaterCounter+1;
        }
        else if(heaterCounter == 0){
            temp =heaterCounter;
        }

    }
    return temp;
}

//returns the current dirt level
uint8_t Simulation::GetDirtLevel(uint8_t washType)
{
    if(washType ==0){
        dirtLevel = QRandomGenerator::global()->bounded(0,3);

    }else if (washType==1){
        dirtLevel = QRandomGenerator::global()->bounded(0,4);

    }
    else if (washType == 3){
        dirtLevel = 0;
    }

    return dirtLevel;
}

//returns the button 10 state
uint8_t Simulation::GetBtn_10(){
    return button10;
}

//returns the button 50 state
uint8_t Simulation::GetBtn_50(){
    return button50;
}

//returns the button 200 state
uint8_t Simulation::GetBtn_200(){
    return button200;
}

//returns the clear button state
uint8_t Simulation::GetBtnClearCoins(){
    return buttonCLR;
}

//returns the switch soap1 state
uint8_t Simulation::GetSwitchSoap1(){
    return buttonSoap1;
}

//returns the switch soap2 state
uint8_t Simulation::GetSwitchSoap2(){
    return buttonSoap2;
}

//returns the program button state
uint8_t Simulation::GetBtnProgram(){
    return program;
}

//returns the start button state
uint8_t Simulation::GetBtnStart(){
    return buttonStart;
}

//Sets the coin 10 level based on the argument
void Simulation::SetCoin_10(uint8_t nrOfCoins){
    if(nrOfCoins == 1){
        ui->coins10LED1->setVisible(1);
        ui->coins10LED2->setVisible(0);
        ui->coins10LED3->setVisible(0);
    }
    else if(nrOfCoins ==2){
        ui->coins10LED1->setVisible(1);
        ui->coins10LED2->setVisible(1);
        ui->coins10LED3->setVisible(0);

    }
    else if(nrOfCoins ==3){
        ui->coins10LED1->setVisible(1);
        ui->coins10LED2->setVisible(1);
        ui->coins10LED3->setVisible(1);
    }
    else if(nrOfCoins == 0){
        ui->coins10LED1->setVisible(0);
        ui->coins10LED2->setVisible(0);
        ui->coins10LED3->setVisible(0);
    }

}

//Sets the coin 50 level based on the argument
void Simulation::SetCoin_50(uint8_t nrOfCoins){
    if(nrOfCoins == 1){
        ui->coins50LED1->setVisible(1);
        ui->coins50LED2->setVisible(0);
        ui->coins50LED3->setVisible(0);
    }
    else if(nrOfCoins ==2){
        ui->coins50LED1->setVisible(1);
        ui->coins50LED2->setVisible(1);
        ui->coins50LED3->setVisible(0);

    }
    else if(nrOfCoins ==3){
        ui->coins50LED1->setVisible(1);
        ui->coins50LED2->setVisible(1);
        ui->coins50LED3->setVisible(1);
    }
    else if(nrOfCoins == 0){
        ui->coins50LED1->setVisible(0);
        ui->coins50LED2->setVisible(0);
        ui->coins50LED3->setVisible(0);
    }
}

//Sets the coin 200 level based on the argument
void Simulation::SetCoin_200(uint8_t nrOfCoins){
    if(nrOfCoins == 1){
        ui->coins200LED1->setVisible(1);
        ui->coins200LED2->setVisible(0);
    }
    else if(nrOfCoins ==2){
        ui->coins200LED1->setVisible(1);
        ui->coins200LED2->setVisible(1);
    }
    else if(nrOfCoins ==0){
        ui->coins200LED1->setVisible(0);
        ui->coins200LED2->setVisible(0);
    }
}

//Sets the program based on the argument
void Simulation::SetProgram(uint8_t setVal){
    if(setVal == 0){
        ui->programLEDA->setVisible(1);
        ui->programLEDB->setVisible(0);
        ui->programLEDC->setVisible(0);
    }
    if(setVal == 1){
        ui->programLEDA->setVisible(0);
        ui->programLEDB->setVisible(1);
        ui->programLEDC->setVisible(0);
    }
    if(setVal == 2){
        ui->programLEDA->setVisible(0);
        ui->programLEDB->setVisible(0);
        ui->programLEDC->setVisible(1);
    }
}

//Sets the soap1 state based on the argument
void Simulation::SetSoap1(uint8_t setVal){

    ui->soap1LED->setVisible(setVal);
    if(setVal==0){
        ui->pushButton_8->setIcon(QIcon("../Images/fsg"));
        ui->pushButton_8->setIconSize(QSize(50,50));
    }else{
        ui->pushButton_8->setIcon(QIcon("../Images/lol"));
        ui->pushButton_8->setIconSize(QSize(50,50));
    }

}

//Sets the soap1 state based on the argument
void Simulation::SetSoap2(uint8_t setVal){

    ui->soap2LED->setVisible(setVal);
    if(setVal==0){
        ui->pushButton_9->setIcon(QIcon("../Images/fsg"));
        ui->pushButton_9->setIconSize(QSize(50,50));
    }else{
        ui->pushButton_9->setIcon(QIcon("../Images/lol"));
        ui->pushButton_9->setIconSize(QSize(50,50));
    }
}

//clears the coins
void Simulation::ClearCoins(){
    SetCoin_10(0);
    SetCoin_50(0);
    SetCoin_200(0);
}

//Sets the internal lock state based on the argument
void Simulation::SetInternalLock(uint8_t setValue)
{
    if (!setValue){
        ui->unlocked->setVisible(1);
        ui->locked->setVisible(0);
    }
    else{
        ui->unlocked->setVisible(0);
        ui->locked->setVisible(1);
    }
}

//returns the door state
uint8_t Simulation::GetSwitchDoorLock()
{
    return buttonDoor;
}

//Sets the drain state based on the argument
void Simulation::SetDrain(uint8_t setVal)
{
    drainState = setVal;
}

//Sets the sink state based on the argument
void Simulation::SetSink(uint8_t setVal)
{
    sinkState = setVal;
}

//return the current water level
uint8_t Simulation::GetWaterLevel()
{
    int temp = 1;
    if(drainState){
        temp=waterCounter-1;
    }
    else if(sinkState){
        if(waterCounter > 0){
            temp =waterCounter+1;
        }
        else if(waterCounter == 0){
            temp =waterCounter;
        }
    }
    return temp;
}

//returns the current water pressure state
uint8_t Simulation::GetWaterPressure()
{
    return waterPressure;
}

//returns the milliseconds passed since the start of the program
unsigned long Simulation::CurrentMillis()
{
    return millis;
}

//returns the current motor rotation
uint8_t Simulation::CurrentRotation()
{
    return !motorDirection;
}

//returns the current
uint8_t Simulation::CurrentSpeed()
{
    return motorSpeed;
}

//Sets the motor speed based on the argument
void Simulation::SetSpeed(int level)
{
    if(motorSpeed!=level){
        motorSpeed = level;
        speedChange = 1;
    }
    if(speedChange){
        if (level == 1){
            timerRotation->stop();
            timerRotation->start(750);
            speedChange = 0;
        }
        else if (level==2){
            timerRotation->stop();
            timerRotation->start(500);
            speedChange = 0;
        }
        else if (level==3){
            timerRotation->stop();
            timerRotation->start(300);
            speedChange = 0;
        }
        else if(level==0){
            timerRotation->stop();
            speedChange = 0;
            Rotate(0);
        }
    }
}

//Sets the motor rotation based on the argument
void Simulation::SetRotation(int direction)
{
    motorDirection = !direction;
}

//Sets the duration to pass based on the argument in seconds
uint8_t Simulation::SetDuration(long duration)
{
    int check =0;
    duration =  duration * 1000;
    if(assignment == 0){
        currentMillis = millis;
        assignment = 1;
    }

    if(millis-currentMillis > duration){
        assignment=0;
        check = 1;
    }
    return check;
}

//handles the door animation and var
void Simulation::on_pushButton_7_clicked()
{
    if (buttonDoor == false){
        buttonDoor =  true;
        ui->pushButton_7->setIcon(QIcon("../Images/lol"));
        ui->pushButton_7->setIconSize(QSize(50,50));

    }
    else if (buttonDoor == true){
        buttonDoor =  false;
        ui->pushButton_7->setIcon(QIcon("../Images/fsg"));
        ui->pushButton_7->setIconSize(QSize(50,50));

    }
}

//handles the pressure animation and var
void Simulation::on_pushButton_2_clicked()
{
    if(waterPressure){
        animation = 1;
        waterPressure = 0;
    }else{
        waterPressure = 1;
        animation =3;
    }
}


//handles the motor rotation based on the argument
void Simulation::Rotate(int led){

    ui->LED_Wash_6->setVisible(0);
    ui->LED_Wash_7->setVisible(0);
    ui->LED_Wash_8->setVisible(0);
    ui->LED_Wash_9->setVisible(0);
    ui->LED_Wash_10->setVisible(0);
    ui->LED_Wash_11->setVisible(0);
    ui->LED_Wash_12->setVisible(0);
    ui->LED_Wash_13->setVisible(0);
    ui->LED_Wash_14->setVisible(0);
    ui->LED_Wash_15->setVisible(0);
    ui->LED_Wash_16->setVisible(0);
    ui->LED_Wash_17->setVisible(0);
    if(led == 0){

    }
    else if(led == 1){
        ui->LED_Wash_6->setVisible(1);

    }
    else if(led == 2){
        ui->LED_Wash_7->setVisible(1);

    }
    else if(led == 3){
        ui->LED_Wash_8->setVisible(1);

    }
    else if(led == 4){
        ui->LED_Wash_9->setVisible(1);

    }
    else if(led == 5){
        ui->LED_Wash_10->setVisible(1);

    }
    else if(led == 6){
        ui->LED_Wash_11->setVisible(1);

    }
    else if(led == 7){
        ui->LED_Wash_12->setVisible(1);

    }
    else if(led == 8){
        ui->LED_Wash_13->setVisible(1);

    }
    else if(led == 9){
        ui->LED_Wash_14->setVisible(1);

    }
    else if(led == 10){
        ui->LED_Wash_15->setVisible(1);

    }
    else if(led == 11){
        ui->LED_Wash_16->setVisible(1);

    }
    else if(led == 12){
        ui->LED_Wash_17->setVisible(1);

    }
}


