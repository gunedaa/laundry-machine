#include "Motor.h"
#include <Arduino.h>

Motor::Motor(IMotor *motor)
{
  m = motor;
  prevMillis = 0;
}

void Motor::Spin(uint8_t direction, uint8_t speed, uint8_t seconds)
{
  if(millis() - prevMillis > seconds)
  {
    m -> SetRotation(direction);
    m -> SetSpeed(speed);
    prevMillis = millis();
  }
}
