#include "Water.h"
#include <Arduino.h>

Water::Water(IWater *water)
{
  w = water;
  Serial.begin(9600);
}

uint8_t Water::IsThereWaterPressure()
{
  Serial.println(w->GetWaterPressure());
  return w->GetWaterPressure();

}

void Water::WaterIn()
{
  w->SetSink(0);
  w->SetDrain(1);
}

void Water::WaterOut()
{
  w->SetDrain(0);
  w->SetSink(1);

}

uint8_t Water::CheckWaterLevel()
{
  Serial.println( w->GetWaterLevel());
  return w->GetWaterLevel();

}
