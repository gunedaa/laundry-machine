#ifndef BUZZER_H
#define BUZZER_H

#include "IBuzzer.h"
#include <stdint.h>

class Buzzer
{
  public:
    Buzzer(IBuzzer *buzzer);
    void Buzz();

  private:
    IBuzzer *b;

};

#endif
