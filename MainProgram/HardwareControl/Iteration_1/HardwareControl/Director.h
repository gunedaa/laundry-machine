#ifndef DIRECTOR_H
#define DIRECTOR_H

#include "Coins.h"
#include "ProgramSelector.h"
#include "Soap.h"
#include "Water.h"
#include "Door.h"
#include "Buzzer.h"
#include <stdint.h>

class Director
{
  public:
    Director(Coins *ic, ProgramSelector *ps, Soap *sp, Water *water, Door *door, Buzzer *buzzer);
    void Work();

  private:
    bool IsReady();
    Coins *c;
    ProgramSelector *p;
    Soap *s;
    Water *w;
    Door *d;
    Buzzer *b;

};

#endif
