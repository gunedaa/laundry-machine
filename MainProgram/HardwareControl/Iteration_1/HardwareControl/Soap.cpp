#include "Soap.h"
#include <Arduino.h>

Soap::Soap(ISoap *soap)
{
  s = soap;
  Serial.begin(9600);
  soapAmountCompartmentOne = 0;
  soapAmountCompartmentTwo = 0;
  reached = 0;
  lastSoapSwitch1 = false;
  currentSoapSwitch1 = false;
  lastSoapSwitch2 = false;
  currentSoapSwitch2 = false;
}

void Soap::CheckSoapInput()
{
  this->ReadSwitch1();
  this->ReadSwitch2();
  this->SetSoapLeds();
}

void Soap::ReadSwitch1()
{

  if (soapAmountCompartmentOne < COMP1MAX )
  {

    currentSoapSwitch1 = s->GetSwitchSoap1();
    if ( currentSoapSwitch1 && !lastSoapSwitch1)
    {
      lastSoapSwitch1 = currentSoapSwitch1;
      soapAmountCompartmentOne++;
      Serial.println(soapAmountCompartmentOne);
    }

    if (!currentSoapSwitch1  && lastSoapSwitch1)
    {
      lastSoapSwitch1 = false;
    }
  }
}
void Soap::ReadSwitch2()
{
  if (soapAmountCompartmentTwo < COMP2MAX )
  {
    currentSoapSwitch2 = s->GetSwitchSoap2();
    if ( currentSoapSwitch2 && !lastSoapSwitch2)
    {
      lastSoapSwitch2 = currentSoapSwitch2;
      soapAmountCompartmentTwo++;
      Serial.println(soapAmountCompartmentTwo);
    }

    if (!currentSoapSwitch2 && lastSoapSwitch2)
    {
      lastSoapSwitch2 = false;
    }
  }
}

void Soap::GiveMeSoap(uint8_t soapCompartment, uint8_t soapUnit)
{
  if (soapCompartment == 1)
  {
    soapAmountCompartmentOne = soapAmountCompartmentOne - soapUnit;
  }
  else if (soapCompartment == 2)
  {
    soapAmountCompartmentTwo = soapAmountCompartmentTwo - soapUnit;
  }
  Serial.println("soapAmountCompartmentOne");
  Serial.println(soapAmountCompartmentOne);
  Serial.println("soapAmountCompartmentTwo");
  Serial.println(soapAmountCompartmentTwo);

}

uint8_t Soap::IsThereEnoughSoap()
{
  /*if (soapAmountCompartmentOne == 2 && soapAmountCompartmentTwo == 3)
    {
    return true;
    }
    else {
    return false;
    }*/
  return (soapAmountCompartmentOne == 2 && soapAmountCompartmentTwo == 3);
}

void Soap::SetSoapLeds()
{
  if (soapAmountCompartmentOne == 2)
  {
    s->SetSoap1(1);
  }
  else if (soapAmountCompartmentOne < 2)
  {
    s->SetSoap1(0);
  }

  if (soapAmountCompartmentTwo == 3)
  {
    s->SetSoap2(1);
  }
  else if (soapAmountCompartmentTwo < 3)
  {
    s->SetSoap2(0);
  }
}
