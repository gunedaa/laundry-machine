#ifndef MOTOR_H
#define MOTOR_H

#include "IMotor.h"
#include <stdint.h>

class Motor
{
  public:
    Motor(IMotor *motor);
    void Spin(uint8_t direction, uint8_t speed, uint8_t seconds);

  private:
    IMotor *m;
    unsigned long prevMillis;
};

#endif
