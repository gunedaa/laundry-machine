#include "Door.h"
#include <Arduino.h>

Door::Door(IDoor *door)
{
  d = door;
}
void Door::LockInternally(uint8_t state)
{
  d->SetInternalLock(state);
}
uint8_t Door::CheckExternalDoorState()
{
  return d->GetSwitchDoorLock();
}
