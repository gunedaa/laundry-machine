#ifndef IBUZZER
#define IBUZZER

#include <stdint.h>

class IBuzzer {

  public:
    virtual void SetBuzzer(uint8_t setVal) = 0;
};

#endif
