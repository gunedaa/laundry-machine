#ifndef IHEATER
#define IHEATER

#include <stdint.h>

class IHeater {

  public:
    virtual void SetHeater(uint8_t setValue) = 0;
    virtual uint8_t GetTemperatureLevel() = 0;
};

#endif
