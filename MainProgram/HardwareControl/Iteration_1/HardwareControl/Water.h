#ifndef WATER_H
#define WATER_H

#include "IWater.h"
#include <stdint.h>

class Water
{
  public:
    Water(IWater *water);
    uint8_t IsThereWaterPressure();
    void WaterIn();
    void WaterOut();
    uint8_t CheckWaterLevel();

  private:
    IWater *w;

};

#endif
