#include "Director.h"
#include "Coins.h"
#include "ProgramSelector.h"
#include "Soap.h"
#include "HardwareControl.h"

HardwareControl *hc;
Coins *cs;
Director *dr;
Soap *sp;
ProgramSelector *ps;
Water *wr;
Door *d;
Buzzer *b;



void setup()
{
  hc = new HardwareControl();
  cs = new Coins(hc);
  ps = new ProgramSelector(hc);
  sp = new Soap(hc);
  wr = new Water(hc);
  d = new Door(hc);
  b = new Buzzer(hc);
  dr = new Director(cs, ps, sp, wr, d, b);

}

void loop()
{
  dr -> Work();
}
