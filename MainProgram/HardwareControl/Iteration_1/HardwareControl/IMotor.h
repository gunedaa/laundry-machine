#ifndef IMOTOR
#define IMOTOR

#include <stdint.h>

class IMotor {

public:
  virtual void SetSpeed(uint8_t level) = 0;
  virtual void SetRotation(uint8_t direction) = 0;
};

#endif
