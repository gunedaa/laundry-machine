#ifndef IPROGRAM
#define IPROGRAM

#include <stdint.h>

class IProgram {

  public:
    virtual void SetProgram(uint8_t setVal);
    virtual uint8_t GetBtnProgram();
    virtual uint8_t GetBtnStart();
};

#endif
