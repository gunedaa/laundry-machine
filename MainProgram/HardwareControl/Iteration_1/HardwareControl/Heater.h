#ifndef HEATER_H
#define HEATER_H

#include "IHeater.h"
#include <stdint.h>

class Heater
{
  public:
    Heater(IHeater *heater);
    void SetHeaterLevel(uint8_t level);
  private:
    IHeater *h;

};

#endif
