#ifndef SOAP_H
#define SOAP_H

#define COMP1MAX 2
#define COMP2MAX 3

#include "ISoap.h"
#include <stdint.h>

class Soap
{
  public:
    Soap(ISoap *soap);
    void GiveMeSoap(uint8_t soapCompartment, uint8_t soapUnit);
    void CheckSoapInput();
    uint8_t IsThereEnoughSoap();

  private:
    ISoap *s;
    void ReadSwitch1();
    void ReadSwitch2();
    void SetSoapLeds();
    uint8_t soapAmountCompartmentOne;
    uint8_t soapAmountCompartmentTwo;
    uint8_t lastSoapSwitch1;
    uint8_t currentSoapSwitch1;
    uint8_t lastSoapSwitch2;
    uint8_t currentSoapSwitch2;
    uint8_t reached;

};

#endif
