#ifndef IDOOR
#define IDOOR

#include <stdint.h>

class IDoor {

  public:
    virtual void SetInternalLock(uint8_t setValue) = 0;
    virtual uint8_t GetSwitchDoorLock() = 0;
};

#endif
