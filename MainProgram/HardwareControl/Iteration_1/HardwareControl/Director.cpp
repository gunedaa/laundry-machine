#include "Director.h"

#include <Arduino.h>

Director::Director(Coins *ic, ProgramSelector *ps, Soap *sp, Water *water, Door *door, Buzzer *buzzer)
{
  p = ps;
  c = ic;
  s = sp;
  w = water;
  d = door;
  b = buzzer;
  Serial.begin(9600);
}

void Director::Work()
{ 
  d->LockInternally(d->CheckExternalDoorState());
  c->CheckCoinsInput();
  p->CheckProgramInput();
  s->CheckSoapInput();

  if (p -> IsStartButtonPressed())
  {
    if (IsReady())
    {
      if (w->IsThereWaterPressure())
      {
        w->WaterIn();
      }
      else
      {
        c -> BlinkAllCoins(3);
      }
    }
    else
    {
      c -> BlinkAllCoins(5);
    }
  }
  
  if (!w->IsThereWaterPressure())
  {
    w->WaterOut();
  }
}

bool Director::IsReady()
{
  //return c->GiveMeBalance() > 0 && s->IsThereEnoughSoap() && p->GetCurrentProgram() != 0 ? true : false;
  if (p->GetCurrentProgram() == 0 && s->IsThereEnoughSoap() && d->CheckExternalDoorState())
  {
    Serial.println("ProgramA");

    if (c->GiveMeBalance() >= 360)
    {
      Serial.println("ProgramA has enough balance");
      s->GiveMeSoap(1, 2);
      c->Withdraw(360);
      return true;
    }
    else {
      return false;
    }
  }

  else if (p->GetCurrentProgram() == 1 && s->IsThereEnoughSoap())
  {
    Serial.println("ProgramB");
    if (c->GiveMeBalance() >= 480)
    {
      Serial.println("ProgramB has enough balance");
      s->GiveMeSoap(1, 2);
      c->Withdraw(480);
      return true;
    }
    else {
      return false;
    }
  }

  else if (p->GetCurrentProgram() == 2 && s->IsThereEnoughSoap())
  {
    Serial.println("ProgramC");
    if (c->GiveMeBalance() >= 510)
    {
      Serial.println("ProgramC has enough balance");
      s->GiveMeSoap(1, 2);
      c->Withdraw(510);
      return true;
    }
    else {
      return false;
    }
  }

  else {
    return false;
  }


}
