#ifndef COINS_H
#define COINS_H

#define COIN10MAX 3
#define COIN50MAX 3
#define COIN200MAX 2

#include "ICoins.h"
#include <stdint.h>

class Coins
{
  public:
    Coins(ICoins *coins);
    int GiveMeBalance();
    void CheckCoinsInput();
    void Withdraw(int amount);

    void BlinkAllCoins(int times);

  private:
    ICoins *ic;
    int totalAmount;
    uint8_t lastBtn10;
    uint8_t lastBtn50;
    uint8_t lastBtn200;
    uint8_t lastBtnClr;
    uint8_t currentBtn10;
    uint8_t currentBtn50;
    uint8_t currentBtn200;
    uint8_t currentBtnClr;
    int coin10;
    int coin50;
    int coin200;

    void ReadCoin10Button();
    void ReadCoin50Button();
    void ReadCoin200Button();
    void ReadClearButton();
    void SetCoinsLeds();


};

#endif
