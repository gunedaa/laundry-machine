#include "Heater.h"
#include <Arduino.h>

Heater::Heater(IHeater *heater)
{
  h = heater;
}

void Heater::SetHeaterLevel(uint8_t level)
{
  h -> SetHeater(level);
}
