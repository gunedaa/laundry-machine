#include "ProgramSelector.h"
#include <Arduino.h>


ProgramSelector::ProgramSelector(IProgram *prog)
{
  p = prog;
  currentProgram = 2;
  lastBtnStart = false;
  currentBtnStart = false;
  lastBtnProg = false;
  currentBtnProg = false;
  Serial.begin(9600);
}

uint8_t ProgramSelector::IsStartButtonPressed()
{
  /*if (p->GetBtnStart())
    {
    return true;
    }*/

  return p->GetBtnStart();
}

void ProgramSelector::CheckProgramInput()
{
  currentBtnProg = p->GetBtnProgram();
  if (currentBtnProg && lastBtnProg == false)
  {
    if (currentProgram < 2)
    {
      lastBtnProg = currentBtnProg;
      currentProgram++;
      p->SetProgram(currentProgram);
      Serial.println(currentProgram);
    }
    else {
      currentProgram = 0;
      p->SetProgram(currentProgram);
      Serial.println(currentProgram);
    }
  }
  if (currentBtnProg == 0 && lastBtnProg == true)
  {
    lastBtnProg = false;
  }
}

ProgramOptions ProgramSelector::GetCurrentProgram()
{
  return currentProgram;

}
