/********************************************/
/* Editors: Reno Picus *********************/
/*          Andonis Roosberg **************/
/*          Eda Gyunesh      *************/
/*          Dimitrije Gutesa ************/
/* Copyright (c). All rights reserved.**/
/**************************************/

#ifndef HardwareControl_H
#define HardwareControl_H

#include <Wire.h>
#include "Centipede.h"
#include "ICoins.h"
#include "IProgram.h"
#include "ISoap.h"
#include "IDoor.h"
#include "IMotor.h"
#include "IBuzzer.h"
#include "IHeater.h"
#include "IWater.h"

#include <Arduino.h>

/* Input-Output lines */
#define OUT_GROUP2 0
#define OUT_GROUP1 1
#define OUT_STROBE 2
#define OUT_KEYSELECT 3
#define OUT_BUZZER 4
#define OUT_HEATER 5
#define OUT_SPEED2 6
#define OUT_SPEED1 7
#define OUT_DATAC 8
#define OUT_DATAB 9
#define OUT_DATAA 10
#define OUT_MOTOR_RL 11
#define OUT_SOAP1 12
#define OUT_SINK 13
#define OUT_DRAIN 14
#define OUT_LOCK 15
#define IN_W2 16
#define IN_W1 17
#define IN_T2 18
#define IN_T1 19
#define IN_IN3 20
#define IN_IN2 21
#define IN_IN1 22
#define IN_IN0 23


/* HardwareControl class is implemented to control the hardware (i.e. LMS). */
class HardwareControl : public ICoins, public IProgram, public ISoap, public IDoor, public IMotor, public IBuzzer, public IWater , public IHeater
{
    Centipede centipede;

  public:
    /*Coins*/
    /**
      Insert some amount of 10 coins. Input possibilites:
        0: no coins are in the machine
        1: one coin is in the machine
        2: two coins are in the machine
        3: three coins are in the machine
    */
    void SetCoin_10(uint8_t nrOfCoins);

    /**
      Insert some amount of 50 coins. Input possibilites:
        0: no coins are in the machine
        1: one coin is in the machine
        2: two coins are in the machine
        3: three coins are in the machine
    */
    void SetCoin_50(uint8_t nrOfCoins);

    /**
      Insert some amount of 200 coins. Input possibilites:
        0: no coins are in the machine
        1: one coin is in the machine
        2: two coins are in the machine
    */
    void SetCoin_200(uint8_t nrOfCoins);

    /**
      Read Coin 10 button
        Returns 1 (1111) if button is pressed
        Returns 0 (1110) if it is not pressed
    */
    uint8_t GetBtn_10();

    /**
      Read Coin 50 button
        Returns 1 (1111) if button is pressed
        Returns 0 (1101) if it is not pressed
    */
    uint8_t GetBtn_50();

    /**
      Read Coin 200 button
        Returns 1 (1111) if button is pressed
        Returns 0 (1011) if it is not pressed
    */
    uint8_t GetBtn_200();

    /**
      Read Clear coins button
        Returns 1 (1111) if button is pressed
        Returns 0 (1000) if it is not pressed
    */
    uint8_t GetBtnClearCoins();

    /**
       Ejects all coins from the machine
    */
    void ClearCoins();

    /*Soap -eda */
    /*
      Turns the Soap1 led on/off depending on the setVal:
        1 -> turn on
        0 -> turn off
    */
    void SetSoap1(uint8_t setVal);

    /*
      Turns the Soap2 led on/off depending on the setVal:
        1 -> turn on
        0 -> turn off
    */
    void SetSoap2(uint8_t setVal);

    /*
      Returns whether Soap1 switch is switched (Soap1 compartment is full):
        1 (HIGH) -> switched (facing upwards)
        0 (LOW) -> not switched (facing downwards)
    */
    uint8_t GetSwitchSoap1();

    /*
      Returns whether Soap2 switch is switched (Soap2 compartment is full):
        1 (HIGH) -> switched (facing upwards)
        0 (LOW) -> not switched (facing downwards)
    */
    uint8_t GetSwitchSoap2();

    /*Heater */

    /*
      Sets the Heater on/off:
        1 -> ON
        0 -> OFF
    */
    void SetHeater(uint8_t setValue);

    /*
      Returns temperature level:
        00 -> 0: cold
        01 -> 1: warm
        10 -> 2: warmer
        11 -> 3: hot
    */
    uint8_t GetTemperatureLevel();



    /*Water -eda */

    /*
      Turns on/off the Drain (Opens/closes the inlet valve):
        1 -> turned on (Starts filling in water)
        0 -> turned off (Stops filling in water)
    */
    void SetDrain(uint8_t setVal);

    /*
      Turns on/off the Sink (Opens/closes the outlet valve):
        1 -> turned on (Starts emptying the tank)
        0 -> turned off (Stops emptying the tank)
    */
    void SetSink(uint8_t setVal);

    /*
      Returns water level:
        00 -> 0: empty
        01 -> 1: 33,3%
        10 -> 2: 66,6%
        11 -> 3: 100% full
    */
    uint8_t GetWaterLevel();

    /*
      Returns water presure:
        0 -> off
        1 -> on
    */
    uint8_t GetWaterPressure();

    /*Buzzer -eda */

    /*
      Turns on/off the buzzer depending on the setVal:
        1 -> Turn on
        0 -> Turn off
    */
    void SetBuzzer(uint8_t setVal);

    /*Program*/

    /**
      Set the program type. Input possibilites:
        0: select program A
        1: select program B
        2: select program C
    */
    void SetProgram(uint8_t setVal);

    /*
      Returns if button program was pressed:
        0 -> false
        1 -> true
    */
    uint8_t GetBtnProgram();

    /*
      Returns if button start was pressed:
        0 -> false
        1 -> true
    */
    uint8_t GetBtnStart();

    /*Door*/

    /**
      Set the internal lock. Input possibilites:
        0: unlocked
        1: locked
    */
    void SetInternalLock(uint8_t setValue);

    /*
      Returns if the door is open or closed:
        0 -> open
        1 -> closed
    */
    uint8_t GetSwitchDoorLock();

    /*Motor*/

    /**
      Set the motor speed. Input possibilites:
        0: off
        1: low speed
        2: medium speed
        3: high speed
    */
    void SetSpeed(uint8_t level);

    /**
      Set the rotation direction. Input possibilites:
        0: counterclockwise
        1: closcwise
    */
    void SetRotation(uint8_t direction);

    /*Setup*/
    HardwareControl();


  private:

    uint8_t led200State = false;
    uint8_t led400State = false;
    uint8_t ledSoap2State = false;

    void Strobe();

    uint8_t currentBtn10 = false;
    uint8_t currentBtn50 = false;
    uint8_t currentBtn200 = false;
    uint8_t currentBtnClear = false;
    uint8_t currentSoapSwitch1 = false;
    uint8_t currentSoapSwitch2 = false;
    uint8_t currentBtnProgram = false;
    uint8_t currentBtnStart = false;
    uint8_t currentSwitchLock = false;

    uint8_t lastBtn10 = false;
    uint8_t lastBtn50 = false;
    uint8_t lastBtn200 = false;
    uint8_t lastBtnClear = false;
    uint8_t lastSoapSwitch1 = false;
    uint8_t lastSoapSwitch2 = false;
    uint8_t lastBtnProgram = false;
    uint8_t lastBtnStart = false;
    uint8_t lastSwitchLock = false;

    unsigned long lastDebounceTime10 = 0;  // the last time the output pin was toggled
    unsigned long lastDebounceTime50 = 0;
    unsigned long lastDebounceTime200 = 0;
    unsigned long lastDebounceTimeClr = 0;
    unsigned long debounceDelay = 50;
};

#endif
