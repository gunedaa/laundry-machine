#ifndef IWATER
#define IWATER

#include <stdint.h>

class IWater {

  public:
    virtual void SetDrain(uint8_t setVal) = 0;
    virtual void SetSink(uint8_t setVal) = 0;
    virtual uint8_t GetWaterLevel() = 0;
    virtual uint8_t GetWaterPressure() = 0;
};

#endif
