#ifndef MOTOR_H
#define MOTOR_H

#include "IMotor.h"
#include <stdint.h>

class Motor
{
  public:
    //constructor
    Motor(IMotor *motor);

    //this method handles spinning the motor in given direction for given duration
    bool Spin(int direction, int speed, unsigned long seconds);

    //
    void Stopwatch();

  private:
    IMotor *m;
    unsigned long prevMillis;

    

};

#endif
