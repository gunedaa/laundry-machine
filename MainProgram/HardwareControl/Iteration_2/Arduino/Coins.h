#ifndef COINS_H
#define COINS_H

#define COIN10MAX 3
#define COIN50MAX 3
#define COIN200MAX 2

#include "ICoins.h"

class Coins
{
  public:
    //This is the constructor of the coins class
    Coins(ICoins *coins);
    //This method is used to Check the current balance
    int GiveMeBalance();
    //This method is used to read the coin buttons and update their LED's accordingly
    void CheckCoinsInput();
    //This method is used to withdraw an amount from the current balance
    void Withdraw(int amount);

  private:
    ICoins *ic;
    int totalAmount;
    uint8_t lastBtn10;
    uint8_t lastBtn50;
    uint8_t lastBtn200;
    uint8_t lastBtnClr;
    uint8_t currentBtn10;
    uint8_t currentBtn50;
    uint8_t currentBtn200;
    uint8_t currentBtnClr;
    int coin10;
    int coin50;
    int coin200;

    void ReadCoin10Button();
    void ReadCoin50Button();
    void ReadCoin200Button();
    void ReadClearButton();
    void SetCoinsLeds();


};

#endif
