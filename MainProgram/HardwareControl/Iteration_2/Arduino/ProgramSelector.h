#ifndef PROGRAMSELECTOR_H
#define PROGRAMSELECTOR_H

#include "IProgram.h"
#include <stdint.h>

enum ProgramOptions
{
  PROGRAM_A,
  PROGRAM_B,
  PROGRAM_C
};

class ProgramSelector
{
  public:
    //constructor
    ProgramSelector(IProgram *prog);

    //this method returns if the button was pressed
    uint8_t IsStartButtonPressed();

    //this method handles choosing a program
    void CheckProgramInput();

    //this method returns current program
    ProgramOptions GetCurrentProgram();

  private:
    IProgram *p;
    uint8_t lastBtnStart;
    uint8_t currentBtnStart;
    uint8_t lastBtnProg;
    uint8_t currentBtnProg;
    uint8_t currentProgram;


};




#endif
