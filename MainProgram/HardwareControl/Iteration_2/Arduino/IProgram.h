#ifndef IPROGRAM
#define IPROGRAM

#include <stdint.h>

class IProgram {

  public:
    virtual void SetProgram(uint8_t setVal) = 0;
    virtual uint8_t GetBtnProgram() = 0;
    virtual uint8_t GetBtnStart() = 0;
};

#endif
