#ifndef IMOTOR
#define IMOTOR

#include <stdint.h>

class IMotor {

  public:
    virtual void SetSpeed(int level) = 0;
    virtual void SetRotation(int direction) = 0;
    virtual uint8_t SetDuration(long duration) = 0;
    virtual uint8_t CurrentRotation() = 0; // Returns the last set rotation
    virtual uint8_t CurrentSpeed() = 0;     // Returns the last set speed
    virtual unsigned long CurrentMillis() = 0; //returns the current millis 
};

#endif
