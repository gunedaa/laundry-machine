#include "Director.h"

Director::Director(Coins *ic, ProgramSelector *ps, ProgramInstructions *ProgramInstructions, Soap *sp, Water *water, Door *door, Buzzer *buzzer, DirtSensor *dirtSensor, Motor *mr, Heater *hr)
{
  p = ps;
  c = ic;
  s = sp;
  w = water;
  d = door;
  b = buzzer;
  pi = ProgramInstructions;
  ds = dirtSensor;
  mt = mr;
  ht = hr;
  DS = IDLE;
}

void Director::Work()
{
  c->CheckCoinsInput();
  switch (DS)
  {
    case (IDLE):
      d->CheckExternalDoorState();
      p->CheckProgramInput();
      s->CheckSoapInput();
      if (p -> IsStartButtonPressed())
      {
        if (IsReady())
        {
          d->LockInternally(1);
          DS = CONSTRUCT;
        }
        else {
          b->Buzz();
        }
      }
      break;

    case (CONSTRUCT):
      wsh = new Wash(this->s, this-> w, this->d, this->b, this->ht, this->mt, this->pi, p->GetCurrentProgram(), this->ds);
      DS = RUN;
      break;

    case (RUN):

      wsh -> DoWash();
      if (wsh->IsWashDone())
      {
        delete wsh;
        DS = IDLE;
      }
      break;
  }
}

bool Director::IsReady()
{
  if ( s->IsThereEnoughSoap() && d->CheckExternalDoorState() && w->IsThereWaterPressure())
  {
    if (c->GiveMeBalance() >= pi->GetProgramInstructions(p->GetCurrentProgram()).progPrice)
    {
      c->Withdraw(pi->GetProgramInstructions(p->GetCurrentProgram()).progPrice);
      return true;
    }
    else {
      return false;
    }
  }

  else {
    return false;
  }

}
