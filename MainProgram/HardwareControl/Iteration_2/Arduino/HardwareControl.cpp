/********************************************/
/* Editors: Reno Picus *********************/
/*          Andonis Roosberg **************/
/*          Eda Gyunesh      *************/
/*          Dimitrije Gutesa ************/
/* Copyright (c). All rights reserved.**/
/**************************************/
#include "HardwareControl.h"

void HardwareControl::Strobe()
{ //replace delay with none blocking code
  centipede.digitalWrite(OUT_STROBE, LOW);
  delay(80);
  centipede.digitalWrite(OUT_STROBE, HIGH);
  delay(10);
}

/*Coins-reno*/
void HardwareControl::SetCoin_10(uint8_t counter) //when the counter is 3, the 200 LED's turn on!
{
  uint8_t dataA = LOW, dataB = LOW, dataC = LOW;
  switch (counter)
  {
    case 0:
      dataA = LOW;
      dataB = LOW;
      dataC = LOW;
      break;
    case 1:
      dataA = HIGH;
      dataB = LOW;
      dataC = LOW;
      break;
    case 2:
      dataA = HIGH;
      dataB = HIGH;
      dataC = LOW;
      break;
    case 3:
      dataA = HIGH;
      dataB = HIGH;
      dataC = HIGH;
      break;
    default:
      break;
  }

  Strobe();
  centipede.digitalWrite(OUT_GROUP1, LOW);
  centipede.digitalWrite(OUT_GROUP2, LOW);
  centipede.digitalWrite(OUT_DATAA, dataA);
  centipede.digitalWrite(OUT_DATAB, dataB);
  centipede.digitalWrite(OUT_DATAC, dataC);
}
void HardwareControl::SetCoin_50(uint8_t counter)
{
  uint8_t dataA = LOW, dataB = LOW, dataC = LOW;
  switch (counter)
  {
    case 0:
      dataA = LOW;
      dataB = LOW;
      dataC = LOW;
      break;
    case 1:
      dataA = HIGH;
      dataB = LOW;
      dataC = LOW;
      break;
    case 2:
      dataA = HIGH;
      dataB = HIGH;
      dataC = LOW;
      break;
    case 3:
      dataA = HIGH;
      dataB = HIGH;
      dataC = HIGH;
      break;
    default:
      break;
  }

  Strobe();
  centipede.digitalWrite(OUT_GROUP1, HIGH);
  centipede.digitalWrite(OUT_GROUP2, LOW);
  centipede.digitalWrite(OUT_DATAA, dataA);
  centipede.digitalWrite(OUT_DATAB, dataB);
  centipede.digitalWrite(OUT_DATAC, dataC);
}

uint8_t HardwareControl::GetDirtLevel(uint8_t washType)
{
  return (!washType) ? rand() % 3 : rand() % 4;
}

void HardwareControl::SetCoin_200(uint8_t counter)
{
  uint8_t dataA = LOW, dataB = LOW, dataC = LOW;

  switch (counter)
  {
    case 0:
      dataA = LOW;
      dataB = LOW;
      dataC = ledSoap2State;
      break;
    case 1:
      dataA = HIGH;
      dataB = LOW;
      dataC = ledSoap2State;
      break;
    case 2:
      dataA = HIGH;
      dataB = HIGH;
      dataC = ledSoap2State;
      break;
    default:
      break;
  }

  Strobe();
  centipede.digitalWrite(OUT_GROUP1, LOW);
  centipede.digitalWrite(OUT_GROUP2, HIGH);
  centipede.digitalWrite(OUT_DATAA, dataA);
  centipede.digitalWrite(OUT_DATAB, dataB);
  centipede.digitalWrite(OUT_DATAC, dataC);

  led200State = dataA;
  led400State = dataB;

}

/*Coins-andy*/

uint8_t HardwareControl::GetBtn_10()
{
  centipede.digitalWrite(OUT_KEYSELECT, HIGH); //set keyselect to high for buttons
  uint8_t firstread = !centipede.digitalRead(IN_IN0) && !centipede.digitalRead(IN_IN1) && !centipede.digitalRead(IN_IN2) && centipede.digitalRead(IN_IN3);
  if (firstread != lastBtn10) 
  {
    lastBtn10 = firstread;
    delay(10);
    uint8_t secondread = !centipede.digitalRead(IN_IN0) && !centipede.digitalRead(IN_IN1) && !centipede.digitalRead(IN_IN2) && centipede.digitalRead(IN_IN3);
    if (firstread && secondread) 
    {
      return true;
    }
  }
  return false;
}

uint8_t HardwareControl::GetBtn_50()
{
  centipede.digitalWrite(OUT_KEYSELECT, HIGH); //set keyselect to high for buttons
  uint8_t firstread = !centipede.digitalRead(IN_IN0) && !centipede.digitalRead(IN_IN1) && centipede.digitalRead(IN_IN2) && !centipede.digitalRead(IN_IN3);
  if (firstread != lastBtn50) 
  {
    lastBtn50 = firstread;
    delay(10);
    uint8_t secondread = !centipede.digitalRead(IN_IN0) && !centipede.digitalRead(IN_IN1) && centipede.digitalRead(IN_IN2) && !centipede.digitalRead(IN_IN3);
    if (firstread && secondread)
    {
      return true;
    }
  }
  return false;
}

uint8_t HardwareControl::GetBtn_200()
{
  centipede.digitalWrite(OUT_KEYSELECT, HIGH); //set keyselect to high for buttons
  uint8_t firstread = !centipede.digitalRead(IN_IN0) && centipede.digitalRead(IN_IN1) && !centipede.digitalRead(IN_IN2) && !centipede.digitalRead(IN_IN3);
  if (firstread != lastBtn200) 
  {
    lastBtn200 = firstread;
    delay(10);
    uint8_t secondread = !centipede.digitalRead(IN_IN0) && centipede.digitalRead(IN_IN1) && !centipede.digitalRead(IN_IN2) && !centipede.digitalRead(IN_IN3);
    if (firstread && secondread)
    {
      return true;
    }
  }
  return false;
}

uint8_t HardwareControl::GetBtnClearCoins()
{
  centipede.digitalWrite(OUT_KEYSELECT, HIGH); //set keyselect to high for buttons
  uint8_t firstread = !centipede.digitalRead(IN_IN0) && centipede.digitalRead(IN_IN1) && centipede.digitalRead(IN_IN2) && centipede.digitalRead(IN_IN3);
  if (firstread != lastBtnClear)
  {
    lastBtnClear = firstread;
    delay(10);
    uint8_t secondread = !centipede.digitalRead(IN_IN0) && centipede.digitalRead(IN_IN1) && centipede.digitalRead(IN_IN2) && centipede.digitalRead(IN_IN3);
    if (firstread && secondread)
    {
      return true;
    }
  }
  return false;
}

void HardwareControl::ClearCoins()
{
  SetCoin_10(LOW);
  SetCoin_50(LOW);
  SetCoin_200(LOW);
}

/*Soap -eda */

void HardwareControl::SetSoap1(uint8_t setVal)
{
  Strobe();
  centipede.digitalWrite(OUT_SOAP1, setVal);
}

void HardwareControl::SetSoap2(uint8_t setVal)
{
  Strobe();
  centipede.digitalWrite(OUT_GROUP1, LOW);
  centipede.digitalWrite(OUT_GROUP2, HIGH);
  centipede.digitalWrite(OUT_DATAA, led200State);
  centipede.digitalWrite(OUT_DATAB, led400State);
  centipede.digitalWrite(OUT_DATAC, setVal);
  ledSoap2State = setVal;
}

uint8_t HardwareControl::GetSwitchSoap2()
{
  centipede.digitalWrite(OUT_KEYSELECT, LOW); //set keyselect to low for switches
  return centipede.digitalRead(IN_IN2);
}

uint8_t HardwareControl::GetSwitchSoap1()
{
  centipede.digitalWrite(OUT_KEYSELECT, LOW); //set keyselect to low for switches
  return centipede.digitalRead(IN_IN1);
}
/* Turns on/off the buzzer depending on the setVal:
   1 -> Turn on
   0 -> Turn off
*/
void HardwareControl::SetBuzzer(uint8_t setValue)
{
  centipede.digitalWrite(OUT_BUZZER, !setValue);
}
void HardwareControl::Delay(int duration)
{
  delay(duration);
}

/*Heater - Dimitrije*/

void HardwareControl::SetHeaterStatus(uint8_t setValue)
{
  centipede.digitalWrite(OUT_HEATER, !setValue);
}

uint8_t HardwareControl::GetCurrentTemperatureLevel()
{
  return centipede.digitalRead(IN_T2) << 1 | centipede.digitalRead(IN_T1);
}

void HardwareControl::SetDrain(uint8_t setVal)
{
  centipede.digitalWrite(OUT_DRAIN, setVal);
}

void HardwareControl::SetSink(uint8_t setVal)
{
  centipede.digitalWrite(OUT_SINK, setVal);
}

uint8_t HardwareControl::GetWaterLevel()
{
  return centipede.digitalRead(IN_W2) << 1 | centipede.digitalRead(IN_W1);
}

uint8_t HardwareControl::GetWaterPressure()
{
  centipede.digitalWrite(OUT_KEYSELECT, LOW); //set keyselect to low for switches
  return centipede.digitalRead(IN_IN0);
}

/*Program -Dimitrije */
void HardwareControl::SetProgram(uint8_t program)
{
  uint8_t dataA = LOW, dataB = LOW, dataC = LOW;
  if (program == 0)
  { //program A
    dataA = HIGH;
  }
  else if (program == 1)
  {
    dataB = HIGH;
  }
  else if (program == 2)
  {
    dataC = HIGH;
  }

  centipede.digitalWrite(OUT_GROUP2, HIGH);
  centipede.digitalWrite(OUT_GROUP1, HIGH);
  Strobe();
  centipede.digitalWrite(OUT_DATAA, dataA);
  centipede.digitalWrite(OUT_DATAB, dataB);
  centipede.digitalWrite(OUT_DATAC, dataC);
}
uint8_t HardwareControl::GetBtnProgram()
{
  centipede.digitalWrite(OUT_KEYSELECT, HIGH); //set keyselect to high for buttons
  uint8_t firstread = centipede.digitalRead(IN_IN0) && centipede.digitalRead(IN_IN3);
  if (firstread != lastBtnProgram)
  {
    lastBtnProgram = firstread;
    delay(10);
    uint8_t secondread = centipede.digitalRead(IN_IN0) && centipede.digitalRead(IN_IN3);
    if (firstread && secondread)
    {
      return true;
    }
  }
  return false;
}
uint8_t HardwareControl::GetBtnStart()
{
  centipede.digitalWrite(OUT_KEYSELECT, HIGH); //set keyselect to high for buttons
  uint8_t firstread = centipede.digitalRead(IN_IN0) && !centipede.digitalRead(IN_IN3);
  if (firstread != lastBtnStart)
  {
    lastBtnStart = firstread;
    delay(10);
    uint8_t secondread = centipede.digitalRead(IN_IN0) && !centipede.digitalRead(IN_IN3);
    if (firstread && secondread)
    {
      return true;
    }
  }
  return false;
}

/*Door-andy*/

void HardwareControl::SetInternalLock(uint8_t setValue)
{
  centipede.digitalWrite(OUT_LOCK, setValue);
}

uint8_t HardwareControl::GetSwitchDoorLock()
{
  centipede.digitalWrite(OUT_KEYSELECT, LOW); //set keyselect to low for switches
  return centipede.digitalRead(IN_IN3);
}

/*Motor-reno*/
void HardwareControl::SetSpeed(int level) {
  uint8_t valueSpeed1 = 0, valueSpeed2 = 0;
  switch (level) {
    case 0:
      valueSpeed1 = 0x01;
      valueSpeed2 = 0x01;
      break;
    case 1:
      valueSpeed1 = 0x00;
      valueSpeed2 = 0x01;
      break;
    case 2:
      valueSpeed1 = 0x01;
      valueSpeed2 = 0x00;
      break;
    case 3:
      valueSpeed1 = 0x00;
      valueSpeed2 = 0x00;
      break;
    default:
      break;
  }
  centipede.digitalWrite(OUT_SPEED1, valueSpeed1);
  centipede.digitalWrite(OUT_SPEED2, valueSpeed2);

  speed = level;
  if (speed == 1)
  {
    delay(1000);
  }
  else if (speed == 2)
  {
    delay(2000);
  }
  else if (speed == 3)
  {
    delay(5000);
  }
}

void HardwareControl::SetRotation(int direction)
{
  centipede.digitalWrite(OUT_MOTOR_RL, direction);
  rotation = direction;
}

uint8_t HardwareControl::SetDuration(long duration)
{
  uint8_t check = 0;
  duration = duration * 1000;
  if (millis() - prevMillis > duration) {
    prevMillis = millis();
    check = 1;
  }
  return check;
}

unsigned long HardwareControl::CurrentMillis()
{
  return millis();
}

uint8_t HardwareControl::CurrentRotation()
{
  return rotation;
}

uint8_t HardwareControl::CurrentSpeed()
{
  return speed;
}

/*Setup-reno*/
HardwareControl::HardwareControl()
{
  //Serial.begin(9600);
  Wire.begin();           // start I2C
  centipede.initialize(); // set all registers to default
  srand(time(NULL));

  for (int i = 0; i <= 15; i++)
  {
    centipede.pinMode(i, OUTPUT);
  }
  for (int i = 16; i <= 23; i++)
  {
    centipede.pinMode(i, INPUT);
  }


  centipede.digitalWrite(OUT_GROUP2, LOW);
  centipede.digitalWrite(OUT_GROUP1, LOW);
  centipede.digitalWrite(OUT_STROBE, LOW);
  centipede.digitalWrite(OUT_KEYSELECT, HIGH);
  centipede.digitalWrite(OUT_BUZZER, HIGH);
  centipede.digitalWrite(OUT_HEATER, HIGH);
  centipede.digitalWrite(OUT_SPEED2, HIGH);
  centipede.digitalWrite(OUT_SPEED1, HIGH);
  centipede.digitalWrite(OUT_DATAC, LOW);
  centipede.digitalWrite(OUT_DATAB, LOW);
  centipede.digitalWrite(OUT_DATAA, LOW);
  centipede.digitalWrite(OUT_MOTOR_RL, LOW);
  centipede.digitalWrite(OUT_SOAP1, LOW);
  centipede.digitalWrite(OUT_SINK, LOW);
  centipede.digitalWrite(OUT_DRAIN, LOW);
  centipede.digitalWrite(OUT_LOCK, LOW);
}
