#ifndef DOOR_H
#define DOOR_H

#include "IDoor.h"

class Door
{
  public:
    //Door contructor
    Door(IDoor *door);
    //This method is used to luck and unlock the internal lock
    void LockInternally(uint8_t state);
    //This method is used to check if the external door is locked or unlocked
    uint8_t CheckExternalDoorState();

  private:
    IDoor *d;

};

#endif
