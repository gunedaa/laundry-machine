#ifndef SOAP_H
#define SOAP_H

#define COMP1MAX 20
#define COMP2MAX 20

#include "ISoap.h"
#include <stdint.h>

class Soap
{
  public:
  //constructor
    Soap(ISoap *soap);

    //this method takes given amount of unit from given soap compartment
    void GiveMeSoap(uint8_t soapCompartment, uint8_t soapUnit);

    //this method checks the soap switches for input
    void CheckSoapInput();

    //this method returns if there is enough soap 
    uint8_t IsThereEnoughSoap();

  private:
    ISoap *s;
    void ReadSwitch1();
    void ReadSwitch2();
    void SetSoapLeds();
    uint8_t soapAmountCompartmentOne;
    uint8_t soapAmountCompartmentTwo;
    uint8_t lastSoapSwitch1;
    uint8_t currentSoapSwitch1;
    uint8_t lastSoapSwitch2;
    uint8_t currentSoapSwitch2;
    uint8_t reached;

};

#endif
