#include "Heater.h"

Heater::Heater(IHeater *heater)
{
  h = heater;
}

bool Heater::SetHeaterLevel(uint8_t temp)
{
  bool response = false;

  if (h->GetCurrentTemperatureLevel() < temp)
  {
    h -> SetHeaterStatus(true);
  }

  if (h-> GetCurrentTemperatureLevel() == temp)
  {
    response = true;
    h -> SetHeaterStatus(false);
  }
  return response;
}

uint8_t Heater::GetTemperatureLevel()
{
  return h->GetCurrentTemperatureLevel();
}

bool Heater::StopHeat()
{
  bool response = false;

  if (h->GetCurrentTemperatureLevel() > 0)
  {
    h -> SetHeaterStatus(false);
  }

  if(h->GetCurrentTemperatureLevel()== 0)
  {
    response = true;
  }
  return response;

}
