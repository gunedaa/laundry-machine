#ifndef BUZZER_H
#define BUZZER_H

#include "IBuzzer.h"

class Buzzer
{
  public:
    //Buzzer constructed
    Buzzer(IBuzzer *buzzer);
    //This method is used to make the buzzer buzz for 1 second
    void Buzz();

  private:
    IBuzzer *b; 
    unsigned long prevMillis;

};

#endif
