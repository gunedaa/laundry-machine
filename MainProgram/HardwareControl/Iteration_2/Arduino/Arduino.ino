#include "Director.h"
#include "Coins.h"
#include "ProgramSelector.h"
#include "Soap.h"
#include "HardwareControl.h"
#include "Wash.h"

HardwareControl *hc;
Coins *cs;
Director *dr;
Soap *sp;
ProgramSelector *ps;
Water *wr;
Door *d;
Buzzer *b;
ProgramInstructions *pi;
DirtSensor *ds;
Heater *ht;
Motor *mt;
//Wash *wsh;


void setup()
{
  hc = new HardwareControl();
  cs = new Coins(hc);
  ps = new ProgramSelector(hc);
  sp = new Soap(hc);
  wr = new Water(hc);
  d = new Door(hc);
  b = new Buzzer(hc);
  pi = new ProgramInstructions();
  ds = new DirtSensor(hc);
  ht = new Heater(hc);
  mt = new Motor(hc);
  dr = new Director(cs, ps, pi, sp, wr, d, b, ds, mt, ht);
}

void loop()
{
  dr -> Work();
}
