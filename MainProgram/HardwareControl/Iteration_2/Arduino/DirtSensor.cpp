#include "DirtSensor.h"


DirtSensor::DirtSensor(IDirtSensor *dirtsensor)
{
  dirtSensor = dirtsensor;
}

uint8_t DirtSensor::CheckDirtLevel(uint8_t washType)
{
  return dirtSensor->GetDirtLevel(washType);

}
