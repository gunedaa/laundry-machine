#ifndef PROGRAMINSTRUCTIONS_H
#define PROGRAMINSTRUCTIONS_H

#include <stdint.h>

//this is a struct with all the program instructions
struct instructions {
  uint8_t preWaterVal;
  uint8_t main1WaterVal;
  uint8_t main2WaterVal;
  uint8_t preTempVal;
  uint8_t main1TempVal;
  uint8_t main2TempVal;
  uint8_t dryTempVal;
  uint8_t preSpeedVal;
  uint8_t main1SpeedVal;
  uint8_t main2SpeedVal;
  uint8_t drySpeedVal;
  uint8_t main1Reps;
  uint8_t main2Reps;
  uint8_t preSpinDir;
  uint8_t main1SpinDir;
  uint8_t main2SpinDir;
  uint8_t dryReps;
  uint8_t drySpinDir;
  int progPrice;

} ;

class ProgramInstructions
{
  public:
    //constructor
    ProgramInstructions();

    //this method returns program instructions from given program
    struct instructions GetProgramInstructions(uint8_t program);

  private:

    instructions ProgramA;
    instructions ProgramB;
    instructions ProgramC;


};

#endif
