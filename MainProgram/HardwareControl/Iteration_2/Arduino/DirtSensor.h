#ifndef DIRTSENSOR_H
#define DIRTSENSOR_H

#include <stdint.h>

#include "IDirtSensor.h"

class DirtSensor
{
  public:
    //Dirtsensor constructor
    DirtSensor(IDirtSensor *dirtsensor);
    //This method returns the dirt level 
    uint8_t CheckDirtLevel(uint8_t washType);

  private:
    IDirtSensor *dirtSensor;

};

#endif
