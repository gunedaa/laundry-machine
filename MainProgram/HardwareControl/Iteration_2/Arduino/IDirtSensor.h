#ifndef IDIRTSENSOR
#define IDIRTSENSOR

#include <stdint.h>

class IDirtSensor {

  public:
    virtual  uint8_t GetDirtLevel(uint8_t washType)=0;
};

#endif
