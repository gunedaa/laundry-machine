#include "Coins.h"

Coins::Coins(ICoins *coins)
{
  ic = coins;
  totalAmount = 0;
  lastBtn10 = false;
  lastBtn50 = false;
  lastBtn200 = false;
  currentBtn10 = false;
  currentBtn50 = false;
  currentBtn200 = false;
  coin10 = 0;
  coin50 = 0;
  coin200 = 0;
}

void Coins::CheckCoinsInput()
{
  this->ReadCoin10Button();
  this->ReadCoin50Button();
  this->ReadCoin200Button();
  this->ReadClearButton();
  this->SetCoinsLeds();
}

void Coins::ReadCoin10Button()
{
  //=============================
  //Button and LED for coin 10 test
  if (coin10 < COIN10MAX)
  {
    currentBtn10 = ic->GetBtn_10();
    if (currentBtn10 && !lastBtn10)
    {
      lastBtn10 = currentBtn10;
      coin10++;
      return;
    }
    if (!currentBtn10 && lastBtn10)
    {
      lastBtn10 = false;
    }
  }

}

void Coins::ReadCoin50Button()
{
  //=============================
  //Button and LED for coin 50 test
  if (coin50 < COIN50MAX)
  {
    currentBtn50 = ic->GetBtn_50();
    if (currentBtn50 && !lastBtn50)
    {
      lastBtn50 = currentBtn50;
      coin50++;

      return;

      //Serial.println("btn50");

    }
    if (!currentBtn50  && lastBtn50)
    {
      lastBtn50 = false;
    }
  }
}

void Coins::ReadCoin200Button()
{
  //==============================
  //Button and LED for coin 200 test
  if (coin200 < COIN200MAX)
  {
    currentBtn200 = ic->GetBtn_200();
    if (currentBtn200 && !lastBtn200)
    {
      lastBtn200 = currentBtn200;
      coin200++;
      return;
    }
    if (!currentBtn200  && lastBtn200)
    {
      lastBtn200 = false;
    }

  }
}

void Coins::SetCoinsLeds()
{
  ic->SetCoin_10(coin10);
  ic->SetCoin_50(coin50);
  ic->SetCoin_200(coin200);
}

void Coins::ReadClearButton()
{
  currentBtnClr = ic->GetBtnClearCoins();
  if (currentBtnClr && !lastBtnClr )
  {
    //this->Withdraw(50);

    ic->ClearCoins();
    coin200 = 0;
    coin50 = 0;
    coin10 = 0;
    
    lastBtnClr = currentBtnClr;
  }
  if (!currentBtnClr && lastBtnClr)
  {
    lastBtnClr = false;
  }
}

int Coins::GiveMeBalance()
{
  totalAmount = coin10 * 10 + coin50 * 50 + coin200 * 200;
  return totalAmount;

}

void Coins::Withdraw(int amount)
{
  totalAmount = totalAmount - amount;

  if (totalAmount >= 200)
  {
    coin200 = totalAmount / 200;
    totalAmount = totalAmount - 200 * coin200;
  }
  else
  {
    coin200 = 0;
  }
  if (totalAmount >= 50)
  {
    coin50 = totalAmount / 50;
    totalAmount = totalAmount - 50 * coin50;
  }
  else
  {
    coin50 = 0;
  }
  if (totalAmount >= 10)
  {
    coin10 = totalAmount / 10;
    totalAmount = totalAmount - 10 * coin10;
  }
  else
  {
    coin10 = 0;
  }

  totalAmount = coin10 * 10 + coin50 * 50 + coin200 * 200;

}
