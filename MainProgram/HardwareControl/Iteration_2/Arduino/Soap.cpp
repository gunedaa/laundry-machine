#include "Soap.h"

Soap::Soap(ISoap *soap)
{
  s = soap;
  soapAmountCompartmentOne = 0; //Soap
  soapAmountCompartmentTwo = 0; //Softner
  reached = 0;
  lastSoapSwitch1 = false;
  currentSoapSwitch1 = false;
  lastSoapSwitch2 = false;
  currentSoapSwitch2 = false;
}

void Soap::CheckSoapInput()
{
  this->ReadSwitch1();
  this->ReadSwitch2();
  this->SetSoapLeds();
}

void Soap::ReadSwitch1()
{

  if (soapAmountCompartmentOne < COMP1MAX )
  {

    currentSoapSwitch1 = s->GetSwitchSoap1();
    if ( currentSoapSwitch1 && !lastSoapSwitch1)
    {
      lastSoapSwitch1 = currentSoapSwitch1;
      soapAmountCompartmentOne = 20;
    }

    if (!currentSoapSwitch1  && lastSoapSwitch1)
    {
      lastSoapSwitch1 = false;
    }
  }
}
void Soap::ReadSwitch2()
{
  if (soapAmountCompartmentTwo < COMP2MAX )
  {
    currentSoapSwitch2 = s->GetSwitchSoap2();
    if ( currentSoapSwitch2 && !lastSoapSwitch2)
    {
      lastSoapSwitch2 = currentSoapSwitch2;
      soapAmountCompartmentTwo = 20;     
    }

    if (!currentSoapSwitch2 && lastSoapSwitch2)
    {
      lastSoapSwitch2 = false;
    }
  }
}

void Soap::GiveMeSoap(uint8_t soapCompartment, uint8_t soapUnit)
{
  if (soapCompartment == 1)
  {
    soapAmountCompartmentOne = soapAmountCompartmentOne - soapUnit;
  }
  else if (soapCompartment == 2)
  {
    soapAmountCompartmentTwo = soapAmountCompartmentTwo - soapUnit;
  }
}

uint8_t Soap::IsThereEnoughSoap()
{
  return (soapAmountCompartmentOne > 4 && soapAmountCompartmentTwo > 1);
}

void Soap::SetSoapLeds()
{
  if (soapAmountCompartmentOne == 20)
  {
    s->SetSoap1(1);
  }
  else if (soapAmountCompartmentOne < 4)
  {
    s->SetSoap1(0);
  }

  if (soapAmountCompartmentTwo == 20)
  {
    s->SetSoap2(1);
  }
  else if (soapAmountCompartmentTwo == 1)
  {
    s->SetSoap2(0);
  }
}
