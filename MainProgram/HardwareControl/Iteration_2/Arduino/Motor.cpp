#include "Motor.h"

Motor::Motor(IMotor *motor)
{
  m = motor;
}

bool Motor::Spin(int direction, int speed, unsigned long seconds)
{

  bool response = false;
  if (m->CurrentRotation() != direction)
  {
    m -> SetRotation(direction);
  }
  if (m->CurrentSpeed() != speed)
  {

    m -> SetSpeed(speed);
  }

  if ((m->CurrentMillis() - prevMillis) > seconds * 1000)
  {
    prevMillis = m->CurrentMillis();
    response = true;
    m -> SetSpeed(0);
  }


  return response;
}

void Motor::Stopwatch()
{
  prevMillis = m->CurrentMillis();
}
