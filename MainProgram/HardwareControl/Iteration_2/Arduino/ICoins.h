#ifndef ICOINS
#define ICOINS

#include <stdint.h>

class ICoins {

public:
  virtual void SetCoin_10(uint8_t nrOfCoins) = 0;
  virtual void SetCoin_50(uint8_t nrOfCoins) = 0;
  virtual void SetCoin_200(uint8_t nrOfCoins) = 0;
  virtual uint8_t GetBtn_10() = 0;
  virtual uint8_t GetBtn_50() = 0;
  virtual uint8_t GetBtn_200() = 0;
  virtual uint8_t GetBtnClearCoins() = 0;
  virtual void ClearCoins() = 0;
};

#endif
