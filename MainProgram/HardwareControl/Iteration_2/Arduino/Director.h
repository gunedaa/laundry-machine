#ifndef DIRECTOR_H
#define DIRECTOR_H

#include "Coins.h"
#include "ProgramSelector.h"
#include "Soap.h"
#include "Water.h"
#include "Door.h"
#include "Buzzer.h"
#include "ProgramInstructions.h"
#include "DirtSensor.h"
#include "Wash.h"



class Director
{
  public:
    //This is the constructor of the director class
    Director(Coins *ic, ProgramSelector *ps, ProgramInstructions *ProgramInstructions, Soap *sp, Water *water, Door *door, Buzzer *buzzer, DirtSensor *dirtSensor, Motor *mr, Heater *hr);
    //this method is used to control the whole washing process
    void Work();

  private:
    //This method is used to check if all the requirments are met
    bool IsReady();
    Heater *ht;
    Motor *mt;
    Coins *c;
    ProgramSelector *p;
    Soap *s;
    Water *w;
    Door *d;
    Buzzer *b;
    ProgramInstructions *pi;
    DirtSensor *ds;
    Wash *wsh;
    ProgramOptions program;

    /*
     * IDLE: Check all inputs
     * CONSTRUCT: Used to construct the washing class
     * RUN: Used to run the washing process
     */
    enum DirectorState {IDLE,CONSTRUCT ,RUN};

    DirectorState DS;


};

#endif
