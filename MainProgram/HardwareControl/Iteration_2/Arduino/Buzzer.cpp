#include "Buzzer.h"

Buzzer::Buzzer(IBuzzer *buzzer)
{
  b = buzzer;
}

void Buzzer::Buzz()
{
  b -> SetBuzzer(1);
  b->Delay(1000);
  b -> SetBuzzer(0);
}
