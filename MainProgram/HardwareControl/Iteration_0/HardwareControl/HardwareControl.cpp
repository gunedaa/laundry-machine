/********************************************/
/* Editors: Reno Picus *********************/
/*          Andonis Roosberg **************/
/*          Eda Gyunesh      *************/
/*          Dimitrije Gutesa ************/
/* Copyright (c). All rights reserved.**/
/**************************************/
#include "HardwareControl.h"

void HardwareControl::Strobe()
{ //replace delay with none blocking code
  centipede.digitalWrite(OUT_STROBE, LOW);
  delay(80);
  centipede.digitalWrite(OUT_STROBE, HIGH);
  delay(10);
}

/*Coins-reno*/
void HardwareControl::SetCoin_10(uint8_t counter) //when the counter is 3, the 200 LED's turn on!
{
  uint8_t dataA = LOW, dataB = LOW, dataC = LOW;
  switch (counter)
  {
    case 0:
      dataA = LOW;
      dataB = LOW;
      dataC = LOW;
      break;
    case 1:
      dataA = HIGH;
      dataB = LOW;
      dataC = LOW;
      break;
    case 2:
      dataA = HIGH;
      dataB = HIGH;
      dataC = LOW;
      break;
    case 3:
      dataA = HIGH;
      dataB = HIGH;
      dataC = HIGH;
      break;
    default:
      break;
  }

  centipede.digitalWrite(OUT_GROUP2, LOW);
  centipede.digitalWrite(OUT_GROUP1, LOW);
  Strobe();
  centipede.digitalWrite(OUT_DATAA, dataA);
  centipede.digitalWrite(OUT_DATAB, dataB);
  centipede.digitalWrite(OUT_DATAC, dataC);
}
void HardwareControl::SetCoin_50(uint8_t counter)
{
  uint8_t dataA = LOW, dataB = LOW, dataC = LOW;
  switch (counter)
  {
    case 0:
      dataA = LOW;
      dataB = LOW;
      dataC = LOW;
      break;
    case 1:
      dataA = HIGH;
      dataB = LOW;
      dataC = LOW;
      break;
    case 2:
      dataA = HIGH;
      dataB = HIGH;
      dataC = LOW;
      break;
    case 3:
      dataA = HIGH;
      dataB = HIGH;
      dataC = HIGH;
      break;
    default:
      break;
  }

  centipede.digitalWrite(OUT_GROUP2, LOW);
  centipede.digitalWrite(OUT_GROUP1, HIGH);
  Strobe();
  centipede.digitalWrite(OUT_DATAA, dataA);
  centipede.digitalWrite(OUT_DATAB, dataB);
  centipede.digitalWrite(OUT_DATAC, dataC);
}
void HardwareControl::SetCoin_200(uint8_t counter)
{
  uint8_t dataA = LOW, dataB = LOW, dataC = LOW;
  switch (counter)
  {
    case 0:
      dataA = LOW;
      dataB = LOW;
      dataC = LOW;
      break;
    case 1:
      dataA = HIGH;
      dataB = LOW;
      dataC = LOW;
      break;
    case 2:
      dataA = HIGH;
      dataB = HIGH;
      dataC = LOW;
      break;
    default:
      break;
  }

  centipede.digitalWrite(OUT_GROUP2, HIGH);
  centipede.digitalWrite(OUT_GROUP1, LOW);
  Strobe();
  centipede.digitalWrite(OUT_DATAA, dataA);
  centipede.digitalWrite(OUT_DATAB, dataB);
  centipede.digitalWrite(OUT_DATAC, dataC);

}

/*Coins-andy*/

uint8_t HardwareControl::GetBtn_10()
{
  centipede.digitalWrite(OUT_KEYSELECT, HIGH); //set keyselect to high for buttons
  int reading = !centipede.digitalRead(IN_IN0) && !centipede.digitalRead(IN_IN1) && !centipede.digitalRead(IN_IN2) && centipede.digitalRead(IN_IN3);
  if (reading != lastBtn10) {
    lastDebounceTime = millis();
  }

  if ((millis() - lastDebounceTime) > debounceDelay) {
    // whatever the reading is at, it's been there for longer than the debounce
    // delay, so take it as the actual current state:

    // if the button state has changed:
    if (reading != currentBtn10) {
      currentBtn10 = reading;

      // only toggle the LED if the new button state is HIGH
      if (currentBtn10 == HIGH) {
        return true;
      }
    }
    else {
       currentBtn10 = LOW;
    }
  }
  lastBtn10 = reading;
}

uint8_t HardwareControl::GetBtn_50()
{
  centipede.digitalWrite(OUT_KEYSELECT, HIGH); //set keyselect to high for buttons
  int reading = !centipede.digitalRead(IN_IN0) && !centipede.digitalRead(IN_IN1) && centipede.digitalRead(IN_IN2) && !centipede.digitalRead(IN_IN3);

  if(reading != lastBtn50)
  {
    lastDebounceTime50 = millis();
  }
  if ((millis() - lastDebounceTime50) > debounceDelay) {
    // whatever the reading is at, it's been there for longer than the debounce
    // delay, so take it as the actual current state:

    // if the button state has changed:
    if (reading != currentBtn50) {
      currentBtn50 = reading;

      // only toggle the LED if the new button state is HIGH
      if (currentBtn50 == HIGH) {
        debounced50 = true;
      }
    }
  }
  lastBtn50 = reading;
  return debounced50;
}

uint8_t HardwareControl::GetBtn_200()
{
  centipede.digitalWrite(OUT_KEYSELECT, HIGH); //set keyselect to high for buttons
  int reading = !centipede.digitalRead(IN_IN0) && centipede.digitalRead(IN_IN1) && !centipede.digitalRead(IN_IN2) && !centipede.digitalRead(IN_IN3);

  if ((millis() - lastDebounceTime200) > debounceDelay) {
    // whatever the reading is at, it's been there for longer than the debounce
    // delay, so take it as the actual current state:

    // if the button state has changed:
    if (reading != currentBtn200) {
      currentBtn200 = reading;

      // only toggle the LED if the new button state is HIGH
      if (currentBtn200 == HIGH) {
        return true;
      }
    }
  }
  lastBtn200 = reading;
}

uint8_t HardwareControl::GetBtnClearCoins()
{
  centipede.digitalWrite(OUT_KEYSELECT, HIGH); //set keyselect to high for buttons
  int reading = !centipede.digitalRead(IN_IN0) && centipede.digitalRead(IN_IN1) && centipede.digitalRead(IN_IN2) && centipede.digitalRead(IN_IN3);

  if ((millis() - lastDebounceTimeClr) > debounceDelay) {
    // whatever the reading is at, it's been there for longer than the debounce
    // delay, so take it as the actual current state:

    // if the button state has changed:
    if (reading != currentBtnClear) {
      currentBtnClear = reading;

      // only toggle the LED if the new button state is HIGH
      if (currentBtnClear == HIGH) {
        return true;
      }
    }
  }
  lastBtnClear = reading;
}

void HardwareControl::ClearCoins()
{
  SetCoin_10(LOW);
  SetCoin_50(LOW);
  SetCoin_200(LOW);
}

/*Soap -eda */

void HardwareControl::SetSoap1(uint8_t setVal)
{
  Strobe();
  centipede.digitalWrite(OUT_SOAP1, setVal);
}

void HardwareControl::SetSoap2(uint8_t setVal)
{
  Strobe();
  centipede.digitalWrite(OUT_GROUP2, HIGH);
  centipede.digitalWrite(OUT_GROUP1, LOW);
  centipede.digitalWrite(OUT_DATAC, setVal);
}

uint8_t HardwareControl::GetSwitchSoap2()
{
  centipede.digitalWrite(OUT_KEYSELECT, LOW); //set keyselect to low for switches
  return centipede.digitalRead(IN_IN2);
}

uint8_t HardwareControl::GetSwitchSoap1()
{
  centipede.digitalWrite(OUT_KEYSELECT, LOW); //set keyselect to low for switches
  return centipede.digitalRead(IN_IN1);
}
/* Turns on/off the buzzer depending on the setVal:
   1 -> Turn on
   0 -> Turn off
*/
void HardwareControl::SetBuzzer(uint8_t setVal)
{
  centipede.digitalWrite(OUT_BUZZER, setVal);
}

/*Heater - Dimitrije*/

void HardwareControl::SetHeater(uint8_t setValue)
{
  centipede.digitalWrite(OUT_HEATER, !setValue);
}

uint8_t HardwareControl::GetTemperatureLevel()
{
  return centipede.digitalRead(IN_T2) << 1 | centipede.digitalRead(IN_T1);
}

void HardwareControl::SetDrain(uint8_t setVal)
{
  centipede.digitalWrite(OUT_DRAIN, setVal);
}

void HardwareControl::SetSink(uint8_t setVal)
{
  centipede.digitalWrite(OUT_SINK, setVal);
}

uint8_t HardwareControl::GetWaterLevel()
{
  return centipede.digitalRead(IN_W2) << 1 | centipede.digitalRead(IN_W1);
}

uint8_t HardwareControl::GetWaterPressure()
{
  centipede.digitalWrite(OUT_KEYSELECT, LOW); //set keyselect to low for switches
  return centipede.digitalRead(IN_IN0);
}

/*Program -Dimitrije */
void HardwareControl::SetProgram(uint8_t program)
{
  uint8_t dataA = LOW, dataB = LOW, dataC = LOW;
  if (program == 0)
  { //program A
    dataA = HIGH;
  }
  else if (program == 1)
  {
    dataB = HIGH;
  }
  else if (program == 2)
  {
    dataC = HIGH;
  }

  centipede.digitalWrite(OUT_GROUP2, HIGH);
  centipede.digitalWrite(OUT_GROUP1, HIGH);
  Strobe();
  centipede.digitalWrite(OUT_DATAA, dataA);
  centipede.digitalWrite(OUT_DATAB, dataB);
  centipede.digitalWrite(OUT_DATAC, dataC);
}
uint8_t HardwareControl::GetBtnProgram()
{
  centipede.digitalWrite(OUT_KEYSELECT, HIGH);
  return centipede.digitalRead(IN_IN0) && centipede.digitalRead(IN_IN3);
}
uint8_t HardwareControl::GetBtnStart()
{
  centipede.digitalWrite(OUT_KEYSELECT, HIGH);
  return centipede.digitalRead(IN_IN0) && !centipede.digitalRead(IN_IN3);
}

/*Door-andy*/

void HardwareControl::SetInternalLock(uint8_t setValue)
{
  centipede.digitalWrite(OUT_LOCK, setValue);
}

uint8_t HardwareControl::GetSwitchDoorLock()
{
  centipede.digitalWrite(OUT_KEYSELECT, LOW); //set keyselect to low for switches
  return centipede.digitalRead(IN_IN3);
}

/*Motor-reno*/
void HardwareControl::SetSpeed(uint8_t level) {
  uint8_t valueSpeed1 = 0, valueSpeed2 = 0;
  switch (level) {
    case 0:
      valueSpeed1 = 0x01;
      valueSpeed2 = 0x01;
      break;
    case 1:
      valueSpeed1 = 0x00;
      valueSpeed2 = 0x01;
      break;
    case 2:
      valueSpeed1 = 0x01;
      valueSpeed2 = 0x00;
      break;
    case 3:
      valueSpeed1 = 0x00;
      valueSpeed2 = 0x00;
      break;
    default:
      break;
  }
  centipede.digitalWrite(OUT_SPEED1, valueSpeed1);
  centipede.digitalWrite(OUT_SPEED2, valueSpeed2);
}

void HardwareControl::SetRotation(uint8_t direction)
{
  centipede.digitalWrite(OUT_MOTOR_RL, direction);
}

/*Setup-reno*/
HardwareControl::HardwareControl()
{
  Wire.begin();           // start I2C
  centipede.initialize(); // set all registers to default

  for (int i = 0; i <= 15; i++)
  {
    centipede.pinMode(i, OUTPUT);
  }
  for (int i = 16; i <= 23; i++)
  {
    centipede.pinMode(i, INPUT);
  }

  centipede.digitalWrite(OUT_GROUP2, LOW);
  centipede.digitalWrite(OUT_GROUP1, LOW);
  centipede.digitalWrite(OUT_STROBE, LOW);
  centipede.digitalWrite(OUT_KEYSELECT, HIGH);
  centipede.digitalWrite(OUT_BUZZER, HIGH);
  centipede.digitalWrite(OUT_HEATER, HIGH);
  centipede.digitalWrite(OUT_SPEED2, HIGH);
  centipede.digitalWrite(OUT_SPEED1, HIGH);
  centipede.digitalWrite(OUT_DATAC, LOW);
  centipede.digitalWrite(OUT_DATAB, LOW);
  centipede.digitalWrite(OUT_DATAA, LOW);
  centipede.digitalWrite(OUT_MOTOR_RL, LOW);
  centipede.digitalWrite(OUT_SOAP1, LOW);
  centipede.digitalWrite(OUT_SINK, LOW);
  centipede.digitalWrite(OUT_DRAIN, LOW);
  centipede.digitalWrite(OUT_LOCK, LOW);
}
