#include "Director.h"

#include <Arduino.h>

Coins *c;
ProgramSelector *p;
Soap *s;

Director::Director(Coins *ic, ProgramSelector *ps, Soap *sp)
{
  p = ps;
  c = ic;
  s = sp;
  Serial.begin(9600);
}

void Director::Work()
{
  c->CheckCoinsInput();
  c->GiveMeBalance();
  //p->CheckProgramInput();
  //s->CheckSoapInput();
}
