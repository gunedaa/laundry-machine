#ifndef DIRECTOR_H
#define DIRECTOR_H

#include "Coins.h"
#include "ProgramSelector.h"
#include "Soap.h"
#include <stdint.h>

class Director
{
  public:
    Director(Coins *ic, ProgramSelector *ps, Soap *sp);
    void Work();
    

  private:
       bool IsReady();
       
};

#endif
