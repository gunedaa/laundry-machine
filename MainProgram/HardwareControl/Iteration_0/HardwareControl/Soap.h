#ifndef SOAP_H
#define SOAP_H

#define COMP1MAX 2
#define COMP2MAX 3

#include "ISoap.h"
#include <stdint.h>

class Soap
{
  public:
    Soap(ISoap *soap);
    void GiveMeSoap(uint8_t soapCompartment, uint8_t soapUnit);
    void CheckSoapInput();
    void ReadSwitch1();
    void ReadSwitch2();
    uint8_t IsThereEnoughSoap();
    void SetSoapLeds();

  private:
    ISoap *s;
    uint8_t soapAmountCompartmentOne;
    uint8_t soapAmountCompartmentTwo;
    uint8_t lastSoapSwitch1;
    uint8_t currentSoapSwitch1;
    uint8_t lastSoapSwitch2;
    uint8_t currentSoapSwitch2;

};

#endif
