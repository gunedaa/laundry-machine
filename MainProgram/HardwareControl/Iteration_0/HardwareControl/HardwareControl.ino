#include "Director.h"
#include "Coins.h"
#include "ProgramSelector.h"
#include "Soap.h"
#include "HardwareControl.h"

HardwareControl *hc;
Coins *cs;
Director *dr;
Soap *sp;
ProgramSelector *ps;



void setup()
{
  hc = new HardwareControl();
  cs = new Coins(hc);
  ps = new ProgramSelector(hc);
  sp = new Soap(hc);
  dr = new Director(cs, ps, sp);
  
}

void loop()
{
  dr -> Work();  
}
