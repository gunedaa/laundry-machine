#include "Coins.h"
#include <Arduino.h>


Coins::Coins(ICoins *coins)
{
  ic = coins;
  totalAmount = 0;
  lastBtn10 = false;
  lastBtn50 = false;
  lastBtn200 = false;
  currentBtn10 = false;
  currentBtn50 = false;
  currentBtn200 = false;
  coin10 = 0;
  coin50 = 0;
  coin200 = 0;
  Serial.begin(9600);
}

void Coins::CheckCoinsInput()
{
  this->ReadCoin10Button();
  this->ReadCoin50Button();
  this->ReadCoin200Button();
  this->ReadClearButton();
  this->SetCoinsLeds();
  this->GiveMeBalance();
}

void Coins::ReadCoin10Button()
{
  //=============================
  //Button and LED for coin 10 test
  if (coin10 < COIN10MAX)
  {
    if (ic->GetBtn_10())
    {
      coin10++;
    } 
  }

}

void Coins::ReadCoin50Button()
{
  //=============================
  //Button and LED for coin 50 test
  if (coin50 < COIN50MAX)
  {
    if (ic->GetBtn_50())
    {
      coin50++;
    }
  }
}

void Coins::ReadCoin200Button()
{
  //==============================
  //Button and LED for coin 200 test
  if (coin200 < COIN200MAX)
  {
    currentBtn200 = ic->GetBtn_200();
    if (currentBtn200 && lastBtn200 == false)
    {
      lastBtn200 = currentBtn200;
      coin200++;
    }
    if (currentBtn200 == 0 && lastBtn200 == true)
    {
      lastBtn200 = false;
    }

  }
}

void Coins::SetCoinsLeds()
{
  ic->SetCoin_10(coin50);
  ic->SetCoin_50(coin200);
  ic->SetCoin_200(coin10);
}

void Coins::ReadClearButton()
{
  currentBtnClr = ic->GetBtnClearCoins();
  if (currentBtnClr && lastBtnClr == false)
  {
    //this->Withdraw(50);

    ic->ClearCoins();
    coin200 = 0;
    coin50 = 0;
    coin10 = 0;

    lastBtnClr = currentBtnClr;
  }
  if (currentBtnClr == 0 && lastBtnClr == true)
  {
    lastBtnClr = false;
  }
}

uint8_t Coins::GiveMeBalance()
{
  totalAmount = coin10 * 10 + coin50 * 50 + coin200 * 200;
  Serial.println(totalAmount);
  return totalAmount;

}

void Coins::Withdraw(uint8_t amount)
{
  totalAmount = totalAmount - amount;

  if (totalAmount >= 200)
  {
    coin200 = totalAmount / 200;
    totalAmount = totalAmount - 200 * coin200;
  }
  else
  {
    coin200 = 0;
  }
  if (totalAmount >= 50)
  {
    coin50 = totalAmount / 50;
    totalAmount = totalAmount - 50 * coin50;
  }
  else
  {
    coin50 = 0;
  }
  if (totalAmount >= 10)
  {
    coin10 = totalAmount / 10;
    totalAmount = totalAmount - 10 * coin10;
  }
  else
  {
    coin10 = 0;
  }

  totalAmount = coin10 * 10 + coin50 * 50 + coin200 * 200;

}
