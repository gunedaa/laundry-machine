#ifndef PROGRAMSELECTOR_H
#define PROGRAMSELECTOR_H

#include "IProgram.h"
#include <stdint.h>

enum ProgramOptions
{
  PROGRAM_A,
  PROGRAM_B,
  PROGRAM_C
};

class ProgramSelector
{
  public:
    ProgramSelector(IProgram *prog);
    uint8_t IsStartButtonPressed();
    void CheckProgramInput();
    ProgramOptions GetCurrentProgram();

  private:
    IProgram *p;
    uint8_t lastBtnStart;
    uint8_t currentBtnStart;
    uint8_t lastBtnProg;
    uint8_t currentBtnProg;
    uint8_t currentProgram;


};




#endif
